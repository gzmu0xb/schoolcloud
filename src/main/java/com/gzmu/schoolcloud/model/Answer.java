package com.gzmu.schoolcloud.model;

public class Answer {
	private Integer answerId;
    private String openid;
    private String answerTopic;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private String answerDaan;
    private String answerChoice;
	public Integer getAnswerId() {
		return answerId;
	}
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getAnswerTopic() {
		return answerTopic;
	}
	public void setAnswerTopic(String answerTopic) {
		this.answerTopic = answerTopic;
	}
	public String getAnswerA() {
		return answerA;
	}
	public void setAnswerA(String answerA) {
		this.answerA = answerA;
	}
	public String getAnswerB() {
		return answerB;
	}
	public void setAnswerB(String answerB) {
		this.answerB = answerB;
	}
	public String getAnswerC() {
		return answerC;
	}
	public void setAnswerC(String answerC) {
		this.answerC = answerC;
	}
	public String getAnswerD() {
		return answerD;
	}
	public void setAnswerD(String answerD) {
		this.answerD = answerD;
	}
	public String getAnswerDaan() {
		return answerDaan;
	}
	public void setAnswerDaan(String answerDaan) {
		this.answerDaan = answerDaan;
	}
	public String getAnswerChoice() {
		return answerChoice;
	}
	public void setAnswerChoice(String answerChoice) {
		this.answerChoice = answerChoice;
	}
    
    
}
