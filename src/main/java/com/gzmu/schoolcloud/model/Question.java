package com.gzmu.schoolcloud.model;

import lombok.Data;


/**
 * 测试题目
 */

@Data
public class Question {
    private Integer id;
    private String question; //问题描述
    private String ture; //正确答案
    private Integer type; //题目类型 1、单选、2多选、3判断
    private Integer scores; //题目分值
    private String cheked; //选项是否选中 ture选中
    private Integer testId; //题目类型 101-1物理实验室 101-2生化实验室、102计算机实验室...
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private String F;

}
