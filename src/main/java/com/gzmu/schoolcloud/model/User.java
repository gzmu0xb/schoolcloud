package com.gzmu.schoolcloud.model;

import lombok.Data;

@Data
public class User {
    private long id;
    private String openid;
    private String name;
    private String gender;
    private String avatarUrl;
    private String nickName;
    private float money;
    private String phone;
    private String time;
    private long role;

    public User(){
        this.id = 0;
        this.openid = "-1";
        this.name = "-1";
        this.gender = "-1";
        this.avatarUrl = "-1";
        this.nickName = "";
        this.money = 0.0f;
        this.phone = "-1";
        this.time = "-1";
        this.role = 0;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public long getRole() {
        return role;
    }

    public void setRole(long role) {
        this.role = role;
    }
}
