package com.gzmu.schoolcloud.model;

public class PrintFile {
     private Long id;
     private String uid;
     private String pid;
     private String oldName;
     private String type;
     private String urlName;
     private String time;
     private Long pages;
     private Long isEnble;

     public Long getId() {
          return id;
     }

     public void setId(Long id) {
          this.id = id;
     }

     public String getUid() {
          return uid;
     }

     public void setUid(String uid) {
          this.uid = uid;
     }

     public String getPid() {
          return pid;
     }

     public void setPid(String pid) {
          this.pid = pid;
     }

     public String getOldName() {
          return oldName;
     }

     public void setOldName(String oldName) {
          this.oldName = oldName;
     }

     public String getType() {
          return type;
     }

     public void setType(String type) {
          this.type = type;
     }

     public String getUrlName() {
          return urlName;
     }

     public void setUrlName(String urlName) {
          this.urlName = urlName;
     }

     public String getTime() {
          return time;
     }

     public void setTime(String time) {
          this.time = time;
     }

     public Long getPages() {
          return pages;
     }

     public void setPages(Long pages) {
          this.pages = pages;
     }

     public Long getIsEnble() {
          return isEnble;
     }

     public void setIsEnble(Long isEnble) {
          this.isEnble = isEnble;
     }
}
