package com.gzmu.schoolcloud.model;

public class AnswerContent {
	private Integer anId;
	private Integer anType;
    private String anTopic;
    private String anA;
    private String anB;
    private String anC;
    private String anD;
    private Integer anTrue;
	public Integer getAnId() {
		return anId;
	}
	public void setAnId(Integer anId) {
		this.anId = anId;
	}
	public Integer getAnType() {
		return anType;
	}
	public void setAnType(Integer anType) {
		this.anType = anType;
	}
	public String getAnTopic() {
		return anTopic;
	}
	public void setAnTopic(String anTopic) {
		this.anTopic = anTopic;
	}
	public String getAnA() {
		return anA;
	}
	public void setAnA(String anA) {
		this.anA = anA;
	}
	public String getAnB() {
		return anB;
	}
	public void setAnB(String anB) {
		this.anB = anB;
	}
	public String getAnC() {
		return anC;
	}
	public void setAnC(String anC) {
		this.anC = anC;
	}
	public String getAnD() {
		return anD;
	}
	public void setAnD(String anD) {
		this.anD = anD;
	}
	public Integer getAnTrue() {
		return anTrue;
	}
	public void setAnTrue(Integer anTrue) {
		this.anTrue = anTrue;
	}
    
	
    
    
}
