package com.gzmu.schoolcloud.model;

public class ImgFile {
     private Long id;
     private String openid;
     private String oldName;
     private String urlName;
     private String time;
     private Long isEnble;

     public Long getId() {
          return id;
     }

     public void setId(Long id) {
          this.id = id;
     }

     public String getOpenid() {
          return openid;
     }

     public void setOpenid(String openid) {
          this.openid = openid;
     }

     public String getOldName() {
          return oldName;
     }

     public void setOldName(String oldName) {
          this.oldName = oldName;
     }

     public String getUrlName() {
          return urlName;
     }

     public void setUrlName(String urlName) {
          this.urlName = urlName;
     }

     public String getTime() {
          return time;
     }

     public void setTime(String time) {
          this.time = time;
     }

     public Long getIsEnble() {
          return isEnble;
     }

     public void setIsEnble(Long isEnble) {
          this.isEnble = isEnble;
     }
}


