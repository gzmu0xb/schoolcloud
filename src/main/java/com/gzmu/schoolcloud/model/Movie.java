package com.gzmu.schoolcloud.model;

public class Movie {
     private Long id;
     private String openid;
     private String oldName;
     private String type;
     private String urlName;
     private String time;
     private Integer downcounts;
     private String branch;
     private String roomName;

     public Long getId() {
          return id;
     }

     public void setId(Long id) {
          this.id = id;
     }

     public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}

	public String getOldName() {
          return oldName;
     }

     public void setOldName(String oldName) {
          this.oldName = oldName;
     }

     public String getType() {
          return type;
     }

     public void setType(String type) {
          this.type = type;
     }

     public String getUrlName() {
          return urlName;
     }

     public void setUrlName(String urlName) {
          this.urlName = urlName;
     }

     public String getTime() {
          return time;
     }

     public void setTime(String time) {
          this.time = time;
     }

     public Integer getDowncounts() {
          return downcounts;
     }

     public void setDowncounts(Integer downcounts) {
          this.downcounts = downcounts;
     }
     
     public String getBranch() {
 		return branch;
 	}

 	public void setBranch(String branch) {
 		this.branch = branch;
 	}

 	public String getRoomName() {
 		return roomName;
 	}

 	public void setRoomName(String roomName) {
 		this.roomName = roomName;
 	}

}
