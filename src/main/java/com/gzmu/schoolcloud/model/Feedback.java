package com.gzmu.schoolcloud.model;

/**
 * 小程序故障反馈实体类
 */
public class Feedback {
	private Integer feId;
	private String openid;
	private String feName;
	private String fePhone;
	private String feText;
	
	public Integer getFeId() {
		return feId;
	}
	public void setFeId(Integer feId) {
		this.feId = feId;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getFeName() {
		return feName;
	}
	public void setFeName(String feName) {
		this.feName = feName;
	}
	
	public String getFePhone() {
		return fePhone;
	}
	public void setFePhone(String fePhone) {
		this.fePhone = fePhone;
	}
	public String getFeText() {
		return feText;
	}
	public void setFeText(String feText) {
		this.feText = feText;
	}
	
	
}
