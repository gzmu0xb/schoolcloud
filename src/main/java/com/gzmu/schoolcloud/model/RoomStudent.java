package com.gzmu.schoolcloud.model;

public class RoomStudent {
	private Integer rmId;
    private String rmName;
    private String rmStudent;
    private String openid;
    private String rmTeacher;
    private String rmPhone;
    private String rmPlace;
    private Integer stat;
    private Integer grade;
    
	public Integer getRmId() {
		return rmId;
	}
	public void setRmId(Integer rmId) {
		this.rmId = rmId;
	}
	public String getRmName() {
		return rmName;
	}
	public void setRmName(String rmName) {
		this.rmName = rmName;
	}
	public String getRmStudent() {
		return rmStudent;
	}
	public void setRmStudent(String rmStudent) {
		this.rmStudent = rmStudent;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	public String getRmTeacher() {
		return rmTeacher;
	}
	public void setRmTeacher(String rmTeacher) {
		this.rmTeacher = rmTeacher;
	}
	public String getRmPhone() {
		return rmPhone;
	}
	public void setRmPhone(String rmPhone) {
		this.rmPhone = rmPhone;
	}
	public String getRmPlace() {
		return rmPlace;
	}
	public void setRmPlace(String rmPlace) {
		this.rmPlace = rmPlace;
	}
	public Integer getStat() {
		return stat;
	}
	public void setStat(Integer stat) {
		this.stat = stat;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	
    
}
