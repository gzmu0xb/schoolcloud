package com.gzmu.schoolcloud.model;

public class Room {
	private int roomId;				//工作室id
	private String openid;			//所属openid
	private String branch;			//所属学院
    private String roomName;		//工作室
	private String roomType;		//工作室类型
	private String roomTime;		//创建时间
    private String roomPlace;		//工作室地点
	private String roomLongitude;	//工作室经度
	private String roomLatitude;	//工作室纬度
	private String roomCampus;		//工作室所属校区
    private String roomTeacher;		//工作室老师
    private String roomEmail;		//工作室邮箱
    private String roomPhone;		//工作室电话
    private String roomImg;			//工作室图片
    private String roomAbout;		//工作室简介
    
    
	
	public int getRoomId() {
		return roomId;
	}
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}
	public String getOpenid() {
		return openid;
	}
	public void setOpenid(String openid) {
		this.openid = openid;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public String getRoomType() {
		return roomType;
	}
	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}
	public String getRoomTime() {
		return roomTime;
	}
	public void setRoomTime(String roomTime) {
		this.roomTime = roomTime;
	}
	public String getRoomPlace() {
		return roomPlace;
	}
	public void setRoomPlace(String roomPlace) {
		this.roomPlace = roomPlace;
	}
	public String getRoomLongitude() {
		return roomLongitude;
	}
	public void setRoomLongitude(String roomLongitude) {
		this.roomLongitude = roomLongitude;
	}
	public String getRoomLatitude() {
		return roomLatitude;
	}
	public void setRoomLatitude(String roomLatitude) {
		this.roomLatitude = roomLatitude;
	}
	public String getRoomCampus() {
		return roomCampus;
	}
	public void setRoomCampus(String roomCampus) {
		this.roomCampus = roomCampus;
	}
	public String getRoomTeacher() {
		return roomTeacher;
	}
	public void setRoomTeacher(String roomTeacher) {
		this.roomTeacher = roomTeacher;
	}
	public String getRoomEmail() {
		return roomEmail;
	}
	public void setRoomEmail(String roomEmail) {
		this.roomEmail = roomEmail;
	}
	public String getRoomPhone() {
		return roomPhone;
	}
	public void setRoomPhone(String roomPhone) {
		this.roomPhone = roomPhone;
	}
	
	public String getRoomImg() {
		return roomImg;
	}
	public void setRoomImg(String roomImg) {
		this.roomImg = roomImg;
	}
	public String getRoomAbout() {
		return roomAbout;
	}
	public void setRoomAbout(String roomAbout) {
		this.roomAbout = roomAbout;
	}
    
    
}
