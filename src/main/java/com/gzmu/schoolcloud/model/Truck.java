package com.gzmu.schoolcloud.model;

public class Truck {
    private int id;
    private String truck;
    private String number;
    private String partition;
    private int status;
    private int engog;
    private String plate;
    private int x;
    private int y;
    private String type;

    public Truck(){
        this.id = 0;
        this.truck = "";
        this.number = "";
        this.partition = "";
        this.status = 0;
        this.engog = 0;
        this.plate = "";
        this.x = 0;
        this.y = 0;
        this.type = "road";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTruck() {return truck; }

    public void setTruck(String truck) {this.truck = truck; }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPartition() {
        return partition;
    }

    public void setPartition(String partition) {
        this.partition = partition;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getEngog() {
        return engog;
    }

    public void setEngog(int engog) {
        this.engog = engog;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
