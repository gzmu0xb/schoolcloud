package com.gzmu.schoolcloud.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.PayMent;

import org.apache.ibatis.annotations.*;

@Mapper
public interface PayMentMapper extends BaseMapper<PayMent> {
    @Select("SELECT * FROM sc_pay WHERE nonceStr=#{nonceStr}")
    PayMent findPayMentByNonceStr(@Param( "nonceStr" ) String nonceStr);

    @Select("SELECT * FROM sc_pay WHERE uid=#{uid}")
    PayMent findPayMentByUid(@Param( "uid" ) String uid);

    @Insert("INSERT INTO sc_pay(uid,state,orders,money,nonceStr,time) " +
            "VALUES (#{uid},#{state},#{orders},#{money},#{nonceStr},#{time})" +
            "ON DUPLICATE KEY UPDATE uid = #{uid},state = #{state},orders = #{orders},money = #{money},nonceStr = #{nonceStr},time = #{time}")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int InsertOrUpPayMent(PayMent user);

}
