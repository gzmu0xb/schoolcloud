package com.gzmu.schoolcloud.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.Cost;
import com.gzmu.schoolcloud.model.PayMent;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface CostMapper extends BaseMapper<PayMent> {
    @Select("SELECT * FROM sc_cost where uid=#{uid}")
    List<Cost> selectCost(@Param("uid") String uid);

    @Insert("INSERT INTO sc_cost(uid,name,price,type,time) " +
            "VALUES (#{uid},#{name},#{price},#{type},#{time})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int InsertCost(Cost user);
}
