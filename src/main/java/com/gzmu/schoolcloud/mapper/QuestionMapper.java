package com.gzmu.schoolcloud.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.gzmu.schoolcloud.model.Question;

import java.util.List;
import java.util.Map;

@Mapper
public interface QuestionMapper {

    @Select("SELECT question,`true`,type,scores,checked FROM `questions` WHERE testId = #{testId}")
    List<Map<String,Object>> findAllQuestionByTestId(@Param("testId") String testId);

    @Select("SELECT A,B,C,D,E,F FROM `questions` WHERE testId = #{testId}")
    List<Map<String,Object>> findAllOptionByTestId(@Param("testId") String testId);
}
