package com.gzmu.schoolcloud.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.transaction.annotation.Transactional;

import com.gzmu.schoolcloud.model.RoomStudent;

@Mapper
public interface RoomStudentMapper {

	@Insert("INSERT INTO room_student(rm_name,rm_student,openid,rm_teacher,rm_phone,rm_place,stat) " +
            "VALUES (#{rmName},#{rmStudent},#{openid},#{rmTeacher},#{rmPhone},#{rmPlace},#{stat})")
    @Options(useGeneratedKeys = true, keyProperty = "rmId", keyColumn = "rm_id")
    int insertRoom(RoomStudent r);
	
	@Update("UPDATE room_student SET grade=#{grade} WHERE openid=#{openid} AND rm_name=#{rmName}")
	@Options(useGeneratedKeys = true, keyProperty = "rmId", keyColumn = "rm_id")
	int updateGrade(RoomStudent student);
	
	//根据openid查询工作室学生
	@Select("SELECT * FROM room_student WHERE openid=#{openid}")
    List<RoomStudent> findAll(@Param("openid") String openid);
	
	//根据openid与工作室名称查询工作室学生（学生查询）
	@Select("SELECT * FROM room_student WHERE openid=#{openid} and stat=#{stat}")
	List<RoomStudent> selectByStat(RoomStudent room);
	
	//根据openid与工作室名称查询工作室学生审核（老师查询审核）
	@Select("SELECT * FROM room_student WHERE rm_teacher=#{rmTeacher} and stat=#{stat}")
    List<RoomStudent> selectAllByName(RoomStudent room);
	
	//根据openid与工作室名称查询工作室学生（学生自己查询）
	@Select("SELECT * FROM room_student WHERE openid=#{openid} and rm_name=#{rmName}")
	RoomStudent selectByName(RoomStudent room);
	
	//根据openid与工作室名称删除工作室学生状态
	@Transactional
	@Delete("Delete from room_student where rm_name=#{rmName} AND openid=#{openid}")
	int deleteStudent(RoomStudent room);

	//根据openid与工作室名称,学生姓名修改工作室学生状态
	@Transactional
	@Update("UPDATE room_student SET stat=#{stat} WHERE rm_name=#{rmName} AND openid=#{openid} AND rm_student=#{rmStudent} ")
	int updateStudent(RoomStudent room);
}
