package com.gzmu.schoolcloud.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.Feedback;
import com.gzmu.schoolcloud.model.PayMent;

public interface FeedbackMapper extends BaseMapper<PayMent>{

	// 添加小程序反馈信息
	@Insert("INSERT INTO feedback(openid,fe_name,fe_phone,fe_text) "
			+ "VALUES (#{openid},#{feName},#{fePhone},#{feText})")
	@Options(useGeneratedKeys = true, keyProperty = "feId", keyColumn = "fe_id")
	int insertFeedback(Feedback feedback);
	
	
}
