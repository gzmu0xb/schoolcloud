package com.gzmu.schoolcloud.mapper;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.User;

import org.apache.ibatis.annotations.*;

@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Select("SELECT * FROM sc_user WHERE openid=#{openid}")
    User findUerByOpenId(@Param("openid") String openid);

    @Insert("INSERT INTO sc_user(openid,nickName,name,gender,avatarUrl,money,phone,time,role) " +
            "VALUES (#{openid},#{nickName},#{name},#{gender},#{avatarUrl},#{money},#{phone},#{time},#{role})" +
            "ON DUPLICATE KEY UPDATE nickName = #{nickName},name = #{name},gender = #{gender},avatarUrl = #{avatarUrl},phone = #{phone},time = #{time},role = #{role}")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int InsertOrUpUser(User user);

    @Select("SELECT max(id) FROM sc_user")
    int getLastUid();

    @Select("SELECT * FROM sc_user WHERE id=#{id}")
    User findUserByUid(@Param("id") int id);

    @Update("UPDATE sc_user set money = #{money} WHERE id=#{id}")
    int upDateUserOfPrice(@Param("id") int uid, @Param("money") double money);
}
