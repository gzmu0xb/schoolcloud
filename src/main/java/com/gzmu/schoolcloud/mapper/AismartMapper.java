package com.gzmu.schoolcloud.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.Aismart;
import com.gzmu.schoolcloud.model.User;

import org.apache.ibatis.annotations.*;

@Mapper
public interface AismartMapper extends BaseMapper<User> {
    @Select("SELECT * FROM sys_aismart")
    Aismart SelectAismart();
}
