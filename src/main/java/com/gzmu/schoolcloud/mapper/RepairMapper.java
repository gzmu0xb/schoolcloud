package com.gzmu.schoolcloud.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.*;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface RepairMapper extends BaseMapper<PayMent> {

    @Select("SELECT branch FROM bx_branch")
    List<String> branchList();

//    @Insert("INSERT INTO bx_user(wxid,openid,nick,name,gender,img,phone,branch,title,role) " +
//            "VALUES (#{wxid},#{openid},#{nick},#{name},#{gender},#{img},#{phone},#{branch},#{title},#{role})" +
//            "ON DUPLICATE KEY UPDATE wxid = #{wxid}, openid = #{openid}, nick = #{nick}, name = #{name}," +
//            "gender = #{gender}, img = #{img}, phone = #{phone}, branch = #{branch}, title = #{title}, role = #{role}")
//    @Options(useGeneratedKeys = true, keyProperty = "openid", keyColumn = "openid")
//    int insertTeacher(Teacher teacher);
    @Insert("INSERT INTO bx_user(openid) VALUES (#{openid})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertTeacher(Teacher teacher);

    @Update("UPDATE bx_user SET wxid=#{wxid}, nickName=#{nickName}, name=#{name}, gender=#{gender}, avatarUrl=#{avatarUrl}, "+
            "phone=#{phone}, branch=#{branch}, title=#{title}, role=#{role} WHERE openid=#{openid}")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int updateTeacher(Teacher teacher);

    @Select("SELECT * FROM bx_user WHERE openid=#{openid}")
    Teacher findUerByOpenId(@Param("openid") String openid);
    
    //根据ID查询表bx_user中的所有信息
    @Select("SELECT * FROM bx_user")
    List<User> selectAll();

    @Select("SELECT max(id) FROM bx_user")
    int getLastUid();

    @Select("SELECT distinct build FROM bx_build")
    List<String> selectAllBuild();
    //distinct 用于返回字段值唯一且不重复的值
    @Select("SELECT distinct floor FROM bx_build WHERE build=#{build}")
    List<String> selectAllFloor(String build);

    @Select("SELECT distinct room FROM bx_build WHERE build=#{build} AND floor=#{floor}")
    List<String> selectAllRoom(Build build);


    @Insert("INSERT INTO bx_file(openid,oldName,urlName,time,isEnble) " +
            "VALUES (#{openid},#{oldName},#{urlName},#{time},#{isEnble})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertImgFile(ImgFile file);

    @Insert("INSERT INTO bx_repair(openid,build,floor,room,facility,tid,tname,timg,tfanlt,ttime,wid,wname,wimg,wtime,wfanlt,latitude,longitude,state) " +
            "VALUES (#{openid},#{build},#{floor},#{room},#{facility},#{tid},#{tname},#{timg},#{tfanlt},#{ttime},#{wid}" +
            ",#{wname},#{wimg},#{wtime},#{wfanlt},#{latitude},#{longitude},#{state})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertRepair(Repair repair);

    @Update("UPDATE bx_repair SET wid=#{wid},wname=#{wname},wimg=#{wimg},wtime=#{wtime},wfanlt=#{wfanlt},latitude=#{latitude},longitude=#{longitude},state=#{state} WHERE id=#{id}")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int updateRepair(Repair repair);

    @Select("SELECT * FROM bx_repair WHERE openid=#{openid} AND state=#{state} order by id DESC limit #{num} ")
    List<Repair> selectRepair(@Param("openid") String openid,@Param("state") Integer state,@Param("num") Integer num);
    
    //根据openid与state完成状态查询上报总数
    @Select("select count(*) from bx_repair WHERE openid=#{openid} AND state=#{state}")
    int selectRepairLength(Repair repair);

    @Select("SELECT * FROM bx_repair WHERE id=#{id}")
    Repair selectRepairById(String id);

    //查询所有待检修的列表（分页显示）
    @Select("SELECT * FROM bx_repair WHERE state=#{state} order by id DESC limit #{num}")
    List<Repair> selectRepairByState(@Param("state") String state,@Param("num") Integer num);

    //根据state查询上报带检修总数
    @Select("select count(*) from bx_repair WHERE state=#{state}")
    int selectRepairStateLength(@Param("state") String state);
    
    @Select("SELECT * FROM bx_repair WHERE  wid=#{wid} AND state=#{state}")
    List<Repair> selectRepairByWidState(Repair repair);

    //把state=0的数加1（统计state=0的总数）取名为wait；把state=1的数加1（统计state=1的总数）取名为finish
    @Select("select sum(case when state=0 then 1 else 0 end) as 'wait',sum(case when state=1 "+
            "then 1 else 0 end) as 'finish' from bx_repair where openid=#{openid}")
    Status totalRepairState(String openid);
}
