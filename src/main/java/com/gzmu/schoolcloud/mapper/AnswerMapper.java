package com.gzmu.schoolcloud.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.Answer;
import com.gzmu.schoolcloud.model.PayMent;

@Mapper
public interface AnswerMapper extends BaseMapper<PayMent> {
	
	/**
	 * 添加答题的信息
	 * @param answer
	 * @return
	 */
	@Insert("INSERT INTO answer(openid,answer_topic,answer_a,answer_b,answer_c,answer_d,answer_daan,answer_choice) " +
            "VALUES (#{openid},#{answerTopic},#{answerA},#{answerB},#{answerC},#{answerD},#{answerDaan},#{answerChoice})")
    @Options(useGeneratedKeys = true, keyProperty = "answer_id", keyColumn = "answerId")
    int insertImgFile(Answer answer);
	
	@Select("SELECT * FROM answer")
    List<String> selectAllBuild();
	
}
