package com.gzmu.schoolcloud.mapper;

import org.apache.ibatis.annotations.*;

import com.gzmu.schoolcloud.model.Printer;

import java.util.List;

@Mapper
public interface PrinterMapper {
    @Insert("INSERT INTO sc_printer(hardid,name,latitude,longitude,other) " +
            "VALUES (#{hardid},#{name},#{latitude},#{longitude},#{other})" +
            "ON DUPLICATE KEY UPDATE name = #{name}, latitude = #{latitude}, longitude = #{longitude}, other = #{other}")
    @Options(useGeneratedKeys = true, keyProperty = "hardid", keyColumn = "hardid")
    int insertOrUpdatePrinter(Printer file);

    @Select("SELECT * FROM sc_printer")
    List<Printer> getAllPrinter();
}
