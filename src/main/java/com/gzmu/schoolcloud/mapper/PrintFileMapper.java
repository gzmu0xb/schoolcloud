package com.gzmu.schoolcloud.mapper;

import org.apache.ibatis.annotations.*;

import com.gzmu.schoolcloud.model.PrintFile;

import java.util.List;

@Mapper
public interface PrintFileMapper {
    @Insert("INSERT INTO sc_file(uid,pid,oldName,type,urlName,time,pages,isEnble) " +
            "VALUES (#{uid},#{pid},#{oldName},#{type},#{urlName},#{time},#{pages},#{isEnble})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertPrintFile(PrintFile file);

    @Select("SELECT * FROM sc_file WHERE uid = #{uid} ORDER BY id desc LIMIT #{page}, #{num}")
    List<PrintFile> getFileOfPages(@Param("uid") int uid, @Param("page") int page, @Param("num") int num);
}
