package com.gzmu.schoolcloud.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.gzmu.schoolcloud.model.Record;

@Mapper
public interface RecordMapper {
	
	@Insert("INSERT INTO sc_record(time,room_name,score,openid) " +
            "VALUES (#{time},#{roomName},#{score},#{openid})")
	@Options(useGeneratedKeys = true, keyProperty = "rid", keyColumn = "rid")
    int addRecord(Record addRecord);
	
	//根据openid查询所有记录（分页显示）
	@Select("SELECT * FROM sc_record where openid=#{openid} order by rid DESC limit #{num}")
    List<Record> selectAllRecord(@Param("openid") String openid,@Param("num") Integer num);
	
	//根据openid查询答题是总数（分页显示）
    @Select("select count(*) from sc_record WHERE openid=#{openid}")
    int RecordLength(@Param("openid") String openid);
}
