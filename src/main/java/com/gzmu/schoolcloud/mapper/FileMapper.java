package com.gzmu.schoolcloud.mapper;

import org.apache.ibatis.annotations.*;

import com.gzmu.schoolcloud.model.File;

import java.util.List;

@Mapper
public interface FileMapper {
    @Insert("INSERT INTO sc_file(openid,oldName,type,urlName,time,downcounts,room_name,branch) " +
            "VALUES (#{openid},#{oldName},#{type},#{urlName},#{time},#{downcounts},#{roomName},#{branch})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertFile(File f);

    @Select("SELECT * FROM sc_file")
    List<File> findAll();
    
    //根据openid查询所有文件信息
    @Select("SELECT * FROM sc_file where openid=#{openid} order by id DESC limit #{num}")
    List<File> findMyAllFile(@Param("openid") String openid,@Param("num") Integer num);
    
    //根据openid查询所有文件长度
    @Select("select count(*) from sc_file where openid=#{openid}")
    int findMyFileLength(@Param("openid") String openid);
    
    //根据roomName查询所有文件信息(分页查询)
    @Select("SELECT * FROM sc_file where room_name=#{roomName} order by id DESC limit #{num} ")
    List<File> findRoomAllFile(@Param("roomName") String roomName,@Param("num") Integer num);

    //根据roomName查询所有文件的长度
    @Select("select count(*) from sc_file where room_name=#{roomName} ")
    int findFileLenth(@Param("roomName") String roomName);
    
    //根据文件用户id获取文件信息
    @Select("select id,uid,pid,oldName,type,urlName,time,downcounts from sc_file where id = #{id}")
    File findById(String id);
    
    //根据文件用户downcounts获取文件信息
    @Select("select downcounts from sc_file where id = #{id}")
    Integer findNum(Integer id);

    //根据id更新下载次数
    @Update("update sc_file set downcounts=#{downcounts} where id=#{id}")
    int updata(File file);
    
    //根据downcounts更新下载次数
    @Update("update sc_file set downcounts=#{downcounts} where id=#{id}")
    int updataNum(@Param("downcounts")Integer downcounts,@Param("id")Integer id);

    //简单的查询
    @Select("select * from sc_file where oldName like '%${fileName}%'")
    List<File> searchFile(@Param("fileName") String fileName);
    
    //删除记录
    @Delete("DELETE FROM sc_file WHERE id=#{id}")
    int dele(@Param("id") String id);
}
