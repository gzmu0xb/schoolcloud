package com.gzmu.schoolcloud.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.Answer;
import com.gzmu.schoolcloud.model.AnswerContent;
import com.gzmu.schoolcloud.model.PayMent;

@Mapper
public interface AnswerContentMapper extends BaseMapper<PayMent> {
	
	/**
	 * 添加答题的信息
	 * @param answer
	 * @return
	 */
	@Insert("INSERT INTO answer(an_type,an_topic,an_a,an_b,an_c,an_d,an_true) " +
            "VALUES (#{anType},#{anTopic},#{anA},#{anB},#{anC},#{anD},#{anTrue})")
    @Options(useGeneratedKeys = true, keyProperty = "an_id", keyColumn = "anId")
    int insertImgFile(Answer answer);
	
	@Select("SELECT * FROM answer_content")
    List<AnswerContent> selectAllAnswerContent();
	
}
