package com.gzmu.schoolcloud.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gzmu.schoolcloud.model.*;

import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ParkMapper extends BaseMapper<PayMent> {
    @Select("SELECT * FROM park_space")
    List<Truck> selecAllPark();

    @Select("SELECT MAX(x) FROM park_space")
    int selecMaxX();

    @Select("SELECT MAX(y) FROM park_space")
    int selecMaxY();

    @Select("SELECT * FROM park_parker WHERE id=#{id}")
    Parker selectParkerById(@Param("id") int id);

    @Update("UPDATE park_space set engog=#{engog} WHERE truck=#{truck}")
    int upDateParkOfOrder(@Param("truck") String truck, @Param("engog") int engog);

    @Select("SELECT * FROM park_user WHERE openid=#{openid}")
    User findUerByOpenId(@Param("openid") String openid);

    @Insert("INSERT INTO park_user(openid,name,gender,avatarUrl,nickName,money,phone,time,role) " +
            "VALUES (#{openid},#{name},#{gender},#{avatarUrl},#{nickName},#{money},#{phone},#{time},#{role})" +
            "ON DUPLICATE KEY UPDATE name = #{name},gender = #{gender},avatarUrl = #{avatarUrl},nickName = #{nickName}," +
            "phone = #{phone},time = #{time},role = #{role}")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int InsertOrUpUser(User user);

    @Select("SELECT * FROM park_user WHERE openid=#{openid}")
    User selectParkUser(@Param("openid") String openid);

    @Insert("INSERT INTO park_order(orders,openid,parkid,plate,truck,ordertime,stoptime,exittime,money,status) " +
            "VALUES (#{orders},#{openid},#{parkid},#{plate},#{truck},#{ordertime},#{stoptime},#{exittime},#{money},#{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    int insertParkOrders(ParkOrder parkOrder);

    @Select("SELECT * FROM park_order LEFT JOIN park_parker on (park_order.parkid = park_parker.id) " +
            "WHERE park_order.`status`=#{status} AND park_order.`openid`=#{openid}")
    ParkOrder selestOrderInfo(@Param("openid") String openid, @Param("status") String status);

    @Select("SELECT * FROM park_order LEFT JOIN park_parker on (park_order.parkid = park_parker.id) " +
            "WHERE park_order.`openid`=#{openid} order by stoptime desc")
    List<ParkOrder> selestOrderList(@Param("openid") String openid);

    @Update("UPDATE park_order set status=#{status}, money=#{money}, exittime=#{exittime} WHERE id=#{id}")
    int cancelOrder(ParkOrder parkOrder);
}
