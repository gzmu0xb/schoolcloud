package com.gzmu.schoolcloud.mapper;

import org.apache.ibatis.annotations.*;

import com.gzmu.schoolcloud.model.Movie;

import java.util.List;

@Mapper
public interface MovieMapper {
    @Insert("INSERT INTO sc_movie(openid,old_name,type,url_name,time,downcounts,branch,room_name) " +
            "VALUES (#{openid},#{oldName},#{type},#{urlName},#{time},#{downcounts},#{branch},#{roomName})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    int insertFile(Movie f);

    //查询所有上传的视频
    @Select("SELECT * FROM sc_movie")
    List<Movie> findAllFile();
    
    //根据openid查询上传的视频
    @Select("SELECT * FROM sc_movie where openid=#{openid}")
    List<Movie> findAllMyMovie(String openid);
    
    //根据roomName查询工作室的视频
    @Select("SELECT * FROM sc_movie where room_name=#{roomName} order by id DESC limit #{num}")
    List<Movie> findRoomAllMovie(@Param("roomName") String roomName,@Param("num") Integer num);
    
    //根据roomName查询所有视频列表的长度
    @Select("SELECT count(*) FROM sc_movie where room_name=#{roomName}")
    int findMovieLenth(@Param("roomName") String roomName);
    
    //根据视频用户id获取文件信息
    @Select("select id,openid,old_name,type,url_name,time,downcounts,branch,room_name from sc_movie where id = #{id}")
    Movie findById(String id);
    
    //根据文件用户downcounts获取文件信息
    @Select("select downcounts from sc_movie where id = #{id}")
    Integer findNum(Integer id);
    
    //根据downcounts更新下载次数
    @Update("update sc_movie set downcounts=#{downcounts} where id=#{id}")
    int updataNum(@Param("downcounts")Integer downcounts,@Param("id")Integer id);
    
    //根据id更新下载次数
    @Update("update sc_movie set downcounts=#{downcounts} where id=#{id}")
    int updata(Movie movie);
    
    //模糊的查询
    @Select("select * from sc_movie where old_name like '%${movieName}%'")
    List<Movie> searchrMovie(@Param("movieName") String movieName);
    
}
