package com.gzmu.schoolcloud.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.gzmu.schoolcloud.model.File;
import com.gzmu.schoolcloud.model.Room;

@Mapper
public interface RoomMapper {

	// 添加工作室信息
	@Insert("INSERT INTO room(room_id,openid,branch,room_name,room_type,room_time,room_place,room_longitude,room_latitude,room_campus,room_teacher,room_email,room_phone,room_img) "
			+ "VALUES (#{roomId},#{openid},#{branch},#{roomName},#{roomType},#{roomTime},#{roomPlace},#{roomLongitude},#{roomLatitude},#{roomCampus},#{roomTeacher},#{roomEmail},#{roomPhone},#{roomImg})")
	@Options(useGeneratedKeys = true, keyProperty = "roomId", keyColumn = "room_id")
	int insertRoom(Room r);

	// 查询所有的工作室名字
	@Select("SELECT room_name FROM room")
	List<String> selectAllRoomName();

	// 查询所有的工作室信息
	@Select("SELECT * FROM room")
	List<Room> findAll();

	// 根据工作室姓名查询工作室信息
	@Select("SELECT * FROM room where room_name=#{roomName} ")
	Room findRoomByName(String roomName);
	
	//模糊查询工作室信息
    @Select("select * from room where room_name like '%${roomName}%'")
    List<Room> dimRoomName(@Param("roomName") String roomName);

	// 根据工作室负责人openid查询对应工作室信息
	@Select("SELECT * FROM room where openid=#{openid} ")
	List<Room> findRoomByOpenid(String openid);

}
