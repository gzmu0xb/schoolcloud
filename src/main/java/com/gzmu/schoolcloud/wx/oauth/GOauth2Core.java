package com.gzmu.schoolcloud.wx.oauth;


import com.alibaba.fastjson.JSONObject;

import com.gzmu.schoolcloud.wx.util.ParamesAPI;
import com.gzmu.schoolcloud.wx.util.WeixinUtil;

public class GOauth2Core {
	public static String GET_CODE = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=CORPID&redirect_uri=REDIRECT_URI&response_type=code&scope=snsapi_base&state=a123#wechat_redirect";

	public static String GetCode(){
		String get_code_url = GET_CODE.replace("CORPID", ParamesAPI.corpId).replace("REDIRECT_URI", WeixinUtil.URLEncoder(ParamesAPI.REDIRECT_URI));
		return get_code_url;
	}
	public static String CODE_TO_USERINFO = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo?access_token=ACCESS_TOKEN&code=CODE&agentid=AGENTID";
	

	public static String GetUserID (String access_token,String code ,String agentid){
		String UserId = "";
		CODE_TO_USERINFO = CODE_TO_USERINFO.replace("ACCESS_TOKEN", access_token).replace("CODE", code).replace("AGENTID", agentid);
		JSONObject jsonobject = WeixinUtil.HttpRequest(CODE_TO_USERINFO, "GET", null);
		if(null!=jsonobject){
			UserId = jsonobject.getString("UserId");
			if(!"".equals(UserId)){
				System.out.println("获取信息成功，———UserID:"+UserId);
			}else{
				int errorrcode = jsonobject.getIntValue("errcode");
	            String errmsg = jsonobject.getString("errmsg");
	            System.out.println("错误码："+errorrcode+"————"+"错误信息："+errmsg);
			}
		}else{
			System.out.println("获取授权失败了，自己找原因。。。");
		}
		return UserId;
	}

}
