package com.gzmu.schoolcloud.wx.contact;


public class MGroup {
	

	public static String CREATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/create?access_token=ACCESS_TOKEN";

	public static String UPDATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/update?access_token=ACCESS_TOKEN";

	public static String DELETE_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/delete?access_token=ACCESS_TOKEN&id=ID";

	public static String GETLIST_URL = "https://qyapi.weixin.qq.com/cgi-bin/department/list?access_token=ACCESS_TOKEN";
	

	public static String Create(String name , String parentid){
		String Postjson = "{\"name\": %s,\"parentid\": %s}";
		return String.format(Postjson, name,parentid);
	}

	public static String Update(String name , String id){
		String Postjson = "{\"id\": %s,\"name\": %s}";
		return String.format(Postjson, name,id);
	}

	public static String Delete(String id){
		String delete_url = DELETE_URL.replace("ID", id);
		return delete_url;
	}
}
