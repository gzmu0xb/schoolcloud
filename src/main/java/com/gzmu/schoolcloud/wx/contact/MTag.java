package com.gzmu.schoolcloud.wx.contact;

public class MTag {

	public static String CREATE_TAG_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/create?access_token=ACCESS_TOKEN";

	public static String UPDATA_TAG_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/update?access_token=ACCESS_TOKEN";

	public static String DELETE_TAG_URL = "https://qyapi.weixin.qq.com/cgi-bin/tag/delete?access_token=ACCESS_TOKEN&tagid=ID";

	public static String GET_TAG_PERSON = "https://qyapi.weixin.qq.com/cgi-bin/tag/get?access_token=ACCESS_TOKEN&tagid=ID";

	public static String ADD_TAG_PERSON = "https://qyapi.weixin.qq.com/cgi-bin/tag/addtagusers?access_token=ACCESS_TOKEN";

	public static String DELETE_TAG_PERSON = "https://qyapi.weixin.qq.com/cgi-bin/tag/deltagusers?access_token=ACCESS_TOKEN";
	

	public static String Create_Tag(String tagname){
		String PostData = "{\"tagname\": %s}";
		return String.format(PostData, tagname);
	}

	public static String Updata_Tag(String tagid , String tagname){
		String PostData = "{\"tagid\": %s,\"tagname\": %s}";
		return String.format(PostData, tagid,tagname);
	}

	public static String Delete_Tag(String tagid){
		String delete_url = DELETE_TAG_URL.replace("ID", tagid);
		return delete_url;
	}

	public static String Get_Tag_Person(String tagid){
		String get_tagperson_url = GET_TAG_PERSON.replace("ID", tagid);
		return get_tagperson_url;
	}

	public static String Add_Tag_Person(String tagid,String userlist){
		String PostData = "{\"tagid\": %s,\"userlist\":%s}";
		return String.format(PostData, tagid,userlist);
	}

	public static String Delete_Tag_Person(String tagid,String userlist){
		String PostData = "{\"tagid\": %s,\"userlist\":%s}";
		return String.format(PostData, tagid,userlist);
	}

}
