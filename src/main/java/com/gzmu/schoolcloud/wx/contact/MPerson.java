package com.gzmu.schoolcloud.wx.contact;


import static com.gzmu.schoolcloud.wx.contact.MGroup.DELETE_URL;

public class MPerson {

	public static String CREATE_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/create?access_token=ACCESS_TOKEN";

	public static String UPDATA_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/update?access_token=ACCESS_TOKEN";


	public static String GET_PERSON_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/get?access_token=ACCESS_TOKEN&userid=ID";

	public static String GET_GROUP_URL = "https://qyapi.weixin.qq.com/cgi-bin/user/simplelist?access_token=ACCESS_TOKEN&department_id=ID&fetch_child=0&status=0";

	public static String Create(String  userid,String name ,String position ,String mobile ,String gender,String tel ,String email,String weixinid){
		String PostData = "{\"userid\": %s,\"name\": %s,\"department\": [1, 2],\"position\": %s,\"mobile\": %s,\"gender\": %s,\"tel\": %s,\"email\": %s,\"weixinid\": %s}";
		return String.format(PostData, userid,name,position,mobile,gender,tel,email,weixinid);
	}
	

	public static String Updata(String  userid,String name ,String position ,String mobile ,String gender,String tel ,String email,String weixinid,String enable){
		String PostData = "{\"userid\": %s,\"name\": %s,\"department\": [1],\"position\": %s,\"mobile\": %s,\"gender\": %s,\"tel\": %s,\"email\": %s,\"weixinid\": %s,\"enable\": %s}";
		return String.format(PostData, userid,name,position,mobile,gender,tel,email,weixinid,enable);
	}
	

	public static String Delete(String userid){
		String delete_url = DELETE_URL.replace("ID", userid);
		return delete_url;
	}

	public static String GPerson(String userid){
		String getperson_url = GET_PERSON_URL.replace("ID", userid);
		return getperson_url;
	}

	public static String GGroup(String department_id){
		String getgroup_url = GET_GROUP_URL.replace("ID", department_id);
		return getgroup_url;
	}

}
