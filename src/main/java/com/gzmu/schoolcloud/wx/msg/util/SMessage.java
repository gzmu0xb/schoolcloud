package com.gzmu.schoolcloud.wx.msg.util;


import com.alibaba.fastjson.JSONArray;

import com.gzmu.schoolcloud.wx.msg.resp.Article;
import com.gzmu.schoolcloud.wx.util.ParamesAPI;
import com.gzmu.schoolcloud.wx.util.WeixinUtil;

import java.util.ArrayList;
import java.util.List;

public class SMessage {
	//发送接口
	public static String POST_URL = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=ACCESS_TOKEN";

	public static String STextMsg(String touser,String toparty,String totag,String agentid,String content){
		String PostData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"totag\":\"%s\",\"msgtype\":\"text\",\"agentid\":%s,\"text\":{\"content\":\"%s\"},\"safe\":\"0\"}";
		return String.format(PostData, touser,toparty,totag,agentid,content);
	}


	public static String SImageMsg(String touser,String toparty,String agentid ,String media_id){
		String PostData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"msgtype\": \"image\",\"agentid\":%s,\"image\":{\"media_id\":\"%s\"},\"safe\":\"0\"}";
		return String.format(PostData, touser,toparty,agentid,media_id);
	}


	public static String SVoiceMsg(String touser,String toparty,String totag,String agentid ,String media_id){
		String PostData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"totag\":\"%s\",\"msgtype\": \"voice\",\"agentid\":%s,\"voice\":{\"media_id\":\"%s\"},\"safe\":\"0\"}";
		return String.format(PostData, touser,toparty,totag,agentid,media_id);
	}

	public static String SVideoMsg(String touser,String toparty,String totag,String agentid,String media_id,String title,String description){
		String PostData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"totag\":\"%s\",\"msgtype\": \"video\",\"agentid\" %s,\"video\": {\"media_id\":\"%s\",\"title\":\"%s\",\"description\":\"%s\"},\"safe\":\"0\"}";
		return String.format(PostData, touser,toparty,totag,agentid,media_id,title,description);
	}

	public static String SFileMsg(String touser,String toparty,String totag,String agentid ,String media_id){
		String PostData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"totag\":\"%s\",\"msgtype\": \"file\",\"agentid\":%s,\"file\":{\"media_id\":\"%s\"},\"safe\":\"0\"}";
		return String.format(PostData, touser,toparty,totag,agentid,media_id);
	}

	public static String SNewsMsg(String touser,String toparty,String totag,String agentid , String articlesList){
		String postData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"totag\":\"%s\",\"msgtype\": \"news\",\"agentid\":%s,\"news\": {\"articles\":%s}}";
		return String.format(postData, touser,toparty,totag,agentid,articlesList);
	}

	public static String SMpNewsMsg(String touser,String toparty,String totag,String agentid , String articlesList){
		String postData = "{\"touser\":\"%s\",\"toparty\":\"%s\",\"totag\":\"%s\",\"msgtype\": \"mpnews\",\"agentid\":%s,\"mpnews\": {\"articles\":%s}\"safe\":\"0\"}";
		return String.format(postData, touser,toparty,totag,agentid,articlesList);
	}
	//示例
	public static void main(String[] args) {
		/**
		 * news示例
		 * */
		// 调取凭证
		String access_token = WeixinUtil.getAccessTokenString( ParamesAPI.corpId, ParamesAPI.secret);
		// 新建图文
		Article article1 = new Article();
		article1.setTitle("news消息测试-1");
		article1.setDescription("");
		article1.setPicurl("http://112.124.111.3/weixinClient/images/weather3.png");
		article1.setUrl("http://112.124.111.3/weixinClient/images/weather3.png");
		Article article2 = new Article();
		article2.setTitle("news消息测试-2");
		article2.setDescription("");
		article2.setPicurl("http://112.124.111.3/weixinClient/images/weather3.png");
		article2.setUrl("http://112.124.111.3/weixinClient/images/weather3.png");
		// 整合图文
		List<Article> list = new ArrayList<Article>();
		list.add(article1);
		list.add(article2);
		// 图文转json
		String articlesList = JSONArray.toJSONString(list);
		// Post的数据
		String PostData = SNewsMsg("UserID1|UserID2|UserID3", "PartyID1 | PartyID2", "TagID1 | TagID2", "1", articlesList);
		int result = WeixinUtil.PostMessage(access_token, "POST", POST_URL, PostData);
		// 打印结果
		if(0==result){
			System.out.println("操作成功");
		}
		else {
			System.out.println("操作失败");
		}
	}
}
