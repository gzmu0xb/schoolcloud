package com.gzmu.schoolcloud.wx.msg.util;


import com.alibaba.fastjson.JSONArray;
import com.gzmu.schoolcloud.wx.msg.resp.Article;
import com.gzmu.schoolcloud.wx.util.ParamesAPI;
import com.gzmu.schoolcloud.wx.util.WeixinUtil;

import java.util.ArrayList;
import java.util.List;

public class SendMsgUtil {
	public static int sendNewsMsg(String title1,String title2,String picurl1,String picurl2,String url,String touser){
		String access_token = WeixinUtil.getAccessTokenString( ParamesAPI.corpId, ParamesAPI.secret);
		Article article1 = new Article();
		article1.setTitle(title1);  
		article1.setDescription("专用于贵州民族大学多媒体设备报修");  
		article1.setPicurl(picurl1);  
		article1.setUrl(url+touser);	
		Article article2 = new Article();
		article2.setTitle(title2);  
		article2.setDescription("为建设一流民族大学而努力");  
		article2.setPicurl(picurl2);  
		article2.setUrl(url+touser);	
		List<Article> list = new ArrayList<Article>();
		list.add(article1);
		list.add(article2);
		String articlesList = JSONArray.toJSONString( list );
		String PostData = SMessage.SNewsMsg(touser,ParamesAPI.Oneself, ParamesAPI.Oneself,ParamesAPI.AgentId, articlesList);
		System.out.println("PostData:"+PostData);
		return WeixinUtil.PostMessage(access_token, "POST", SMessage.POST_URL, PostData);
	}
}
