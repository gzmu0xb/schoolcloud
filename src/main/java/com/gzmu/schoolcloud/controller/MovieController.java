package com.gzmu.schoolcloud.controller;

import com.gzmu.schoolcloud.mapper.MovieMapper;
import com.gzmu.schoolcloud.model.Movie;
import com.alibaba.fastjson.JSONObject;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@Controller
public class MovieController {

    @Autowired
    private MovieMapper movieMapper;

    @RequestMapping("/upmovie")
    public String upfile(HttpServletRequest request, Model model) {
        String uid = request.getParameter("uid");

        System.out.println("++++upmovie uid:" + uid );
        if (uid != null)
            model.addAttribute("uid", uid);
        return "/app/jieyin";
    }

    @ResponseBody
    @PostMapping("/uploadMovie")
    public String upload(@RequestParam("file") MultipartFile file,HttpServletRequest request) {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String openid = request.getParameter("openid");
        String branch = request.getParameter("branch");
        String roomName = request.getParameter("roomName");
        
        String path = "E:/schoolcloud/" ;
        //获取文件名
        String movieName = request.getParameter("movieName");
        System.out.println("上传文件名："+movieName);
        System.out.println("62++++upload openid:" + openid);
        
        if (movieName==null)
        	movieName = file.getOriginalFilename();
        //随机生成文件名，避免重复
        String newName = UUID.randomUUID().toString();
        // 获取系统文件分隔符
        String type = movieName.substring(movieName.lastIndexOf(".") + 1);
        String name = movieName.substring(0,movieName.lastIndexOf("."));
        System.out.println("第二个上传文件名："+movieName);
        //格式化日期
        String nowDate = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String timeStr = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        try {
            //文件路径
            java.io.File epath = new java.io.File(path);
            //System.out.println("根目录:" + path.getAbsolutePath());
            java.io.File uploadPath = new java.io.File(epath.getAbsolutePath(),"static/upfile/" + nowDate + "/") ;
            //文件夹路径不存在
            System.out.println("文件存储路径："+uploadPath);
            if (!uploadPath.exists() && !uploadPath.isDirectory()) {
                System.out.println("创建路径:" + uploadPath);
                uploadPath.mkdirs();
                System.out.println("文件存储路径："+uploadPath);
            }

            String nPath = uploadPath +"/"+ newName + "." + type;
            System.out.println("npath"+nPath);
            java.io.File newFile = new java.io.File(nPath);
            System.out.println("newFile"+newFile);
            file.transferTo(newFile);
            //System.out.println("pages:" + pages);
            //把文件信息存储到数据库
            String fileUrl = "/upfile/" + nowDate + "/" + newName + "." + type;
            System.out.println("104fileUrl:"+fileUrl);
            Movie f = new Movie();
            f.setOpenid(openid);
            f.setType(type);
            f.setOldName(name);
            f.setUrlName(fileUrl);
            f.setTime(timeStr);
            f.setDowncounts(0);
            f.setBranch(branch);
            f.setRoomName(roomName);
            
            System.out.println("104reOpenid:"+openid);
            System.out.println("104branch:"+branch);
            System.out.println("104roomName:"+roomName);
            Map<String, Object> map = new HashMap<String, Object>();
            int reValue = movieMapper.insertFile(f);
            System.out.println("104reValue:"+reValue);
            //判断数据存储是否成功
            if (reValue==1){
                System.out.println(movieName + " upload success");
                map.put("result",true);
            }else{
                System.out.println(movieName + " upload fail");
                map.put("result",false);
            }
            map.put("name",movieName);
            map.put("path",fileUrl);
            return JSONObject.toJSON(map).toString();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return "upload fail";
    }

    @ResponseBody
    @RequestMapping("/findAllMovie")
    public List<Movie> findAllJSON( Model model) {
        //查询所有的文件信息
        List<Movie> userFiles = movieMapper.findAllFile();
        model.addAttribute("files", userFiles);
        System.out.println(userFiles);
        return userFiles;
    }
    
    //根据openid查出自己发布的视频
    @ResponseBody
    @RequestMapping("/findAllMyMovie")
    public String findAllMyMovie(HttpServletRequest request) {
    	String openid = request.getParameter("openid");
    	System.out.println("openid:"+openid);
        //查询所有的文件信息
        List<Movie> myMovies = movieMapper.findAllMyMovie(openid);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("myMovies",myMovies);
        System.out.println(myMovies);
        return JSONObject.toJSON(map).toString();
    }
    
    //根据roomName查出自己发布的文件视频
    @ResponseBody
    @RequestMapping("/findAllRoomMovie")
    public String findAllRoomMovie(HttpServletRequest request) {
    	String roomName = request.getParameter("roomName");
    	String nu = request.getParameter("num");		//接收一定的文件数量
    	System.out.println("152roomName:"+roomName);
    	Integer num = Integer.valueOf(nu);
        //查询所有的视频信息
        List<Movie> roomNameMovies = movieMapper.findRoomAllMovie(roomName,num);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("roomNameMovies",roomNameMovies);
        System.out.println(roomNameMovies);
        return JSONObject.toJSON(map).toString();
    }
    
    //根据roomName查出所有视频列表的长度
    @ResponseBody
    @RequestMapping("/findAllMovieLenth")
    public String findAllMovieLength(HttpServletRequest request) {
    	String roomName = request.getParameter("roomName");
    	System.out.println("173roomName:"+roomName);
        //查询所有的文件长度根据roomName
        int findMovieLenth = movieMapper.findMovieLenth(roomName);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("findMovieLenth",findMovieLenth);
        System.out.println(findMovieLenth);
        return JSONObject.toJSON(map).toString();
    }

    //下载视频
    @ResponseBody
    @RequestMapping("downloadMovies")
    public void downloadMovies(String openStyle, String id, HttpServletResponse response) throws IOException {
        //获取打开方式
        openStyle = openStyle == null ? "attachment" : openStyle;
        //获取文件信息
        Movie movie = movieMapper.findById(id);
        //点击下载链接更新下载次数
        if ("attachment".equals(openStyle)) {
            movie.setDowncounts(movie.getDowncounts() + 1);
            movieMapper.updata(movie);
        }
        
        //根据文件信息中文件名字 和 文件存储路径获取文件输入流
        //文件路径
        java.io.File path = new java.io.File("E:/schoolcloud/");
        System.out.println("根目录:" + path);
        String realpath = path + "/static/" + movie.getUrlName();
        System.out.println("++++++文件请求下载的地址："+realpath);
        //获取文件输入流
        FileInputStream is = new FileInputStream(new java.io.File(realpath));
        //附件下载
        response.setHeader("content-disposition", openStyle + ";fileName=" + URLEncoder.encode(movie.getOldName(), "UTF-8"));
        //获取响应输出流
        ServletOutputStream os = response.getOutputStream();
        //文件拷贝
        IOUtils.copy(is, os);
        IOUtils.closeQuietly(is);
        IOUtils.closeQuietly(os);
    }
    
    //更新下载次数
    @ResponseBody
    @RequestMapping("/updateMovieNum")
    public String updateMovieNum(HttpServletRequest request) {
    	String rid = request.getParameter("id");
    	System.out.println("166id:"+rid);
    	Integer id = Integer.valueOf(rid);
        //查询所有的文件长度根据roomName
        Integer findNum = movieMapper.findNum(id);
        System.out.println("findNum:"+findNum);
        Integer downcounts = findNum + 1;
        System.out.println("downcounts:"+downcounts);
        int updataNum = movieMapper.updataNum(downcounts,id);
        System.out.println("updataNum:"+updataNum);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("updataNum",updataNum);
        System.out.println(updataNum);
        return JSONObject.toJSON(map).toString();
    }

    //搜索文件
    @ResponseBody
    @RequestMapping("/searchrMovie")
    public List<Movie> searchrMovie( Model model,String movieName) {
        System.out.println("搜索的关键字："+movieName);
        //查询所有的文件信息
        List<Movie> userMovies = movieMapper.searchrMovie(movieName);
        //赋值
        model.addAttribute("movies", userMovies);
        System.out.println(userMovies);
        return userMovies;
    }

}
