package com.gzmu.schoolcloud.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.gzmu.schoolcloud.mapper.RoomStudentMapper;
import com.gzmu.schoolcloud.model.RoomStudent;

@Controller
public class RoomStudentController {

	@Autowired
	private RoomStudentMapper roomStudentMapper;
	
	/**
	 * 根据openid查询工作室学生
	 * @param openid 学生openid
	 * @return 成功返回对应的工作室
	 */
	@ResponseBody
    @PostMapping("/RoomStudent")
    public String RoomStudent(@RequestParam("openid") String openid) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        
        System.out.println("++++工作室学生openid:" + openid);
        List<RoomStudent> student = roomStudentMapper.findAll(openid);
        map.put("student",student);
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 根据openid与工作室名称查询工作室学生信息
	 * @param request 页面传过来的值
	 * @return 成功返回RoomStudent类型
	 */
	@ResponseBody
    @RequestMapping("/RoomStudentByName")
    public String RoomStudentByName(HttpServletRequest request) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //获取页面传过来的openid
        String openid = request.getParameter("openid");
        String rmName = request.getParameter("rmName");
        System.out.println("++++工作室学生openid:" + request.getParameter("openid"));
        System.out.println("++++工作室学生rmName:" + rmName);
        RoomStudent room = new RoomStudent();
        room.setOpenid(openid);
        room.setRmName(rmName);
        RoomStudent selectRoomByName = roomStudentMapper.selectByName(room);
        map.put("selectRoomByName",selectRoomByName);
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 根据stat与工作室名称查询工作室学生信息（教师查询）
	 * @param request 页面传过来的值
	 * @return 成功返回RoomStudent类型
	 */
	@ResponseBody
    @RequestMapping("/RoomStudentByNameAll")
    public String RoomStudentByNameAll(HttpServletRequest request) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //获取页面传过来的值
        String rmTeacher = request.getParameter("rmTeacher");
        String stat = request.getParameter("stat");
        System.out.println("++++工作室学生stat:" + request.getParameter("stat"));
        System.out.println("++++工作室老师rmTeacher:" + rmTeacher);
        RoomStudent room = new RoomStudent();
        room.setRmTeacher(rmTeacher);
        room.setStat(Integer.valueOf(stat));
        List<RoomStudent> selectAllByName = roomStudentMapper.selectAllByName(room);
        map.put("selectAllByName",selectAllByName);
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 根据openid与stat=1查询工作室学生
	 * @param request 页面openid与stat
	 * @return 成功返回list集合
	 */
	@ResponseBody
    @RequestMapping("/selectStudentByStat")
    public String selectStudentByStat(HttpServletRequest request) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //获取页面传过来的openid
        String openid = request.getParameter("openid");
        String stat = request.getParameter("stat");
        System.out.println("++++工作室学生openid:" + request.getParameter("openid"));
        System.out.println("++++工作室学生rmName:" + stat);
        RoomStudent room = new RoomStudent();
        room.setOpenid(openid);
        room.setStat(Integer.valueOf(stat));
        List<RoomStudent> selectByStat = roomStudentMapper.selectByStat(room);
        map.put("selectByStat", selectByStat);
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 添加申请加入的工作室学生
	 * @param request 新的学生
	 * @return 成功返回大于等于1，失败返回小于等于0的整数
	 */
	@ResponseBody
	@RequestMapping("/InsertStudent")
    public String InsertStudent(HttpServletRequest request) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //获取页面传过来的值
        String rmName = request.getParameter("rmName");
        String rmStudent = request.getParameter("rmStudent");
        String openid = request.getParameter("openid");
        String rmTeacher = request.getParameter("rmTeacher");
        String rmPhone = request.getParameter("rmPhone");
        String rmPlace = request.getParameter("rmPlace");
        Integer stat = 0;
        System.out.println("++++工作室学生openid:" + request.getParameter("openid"));
        System.out.println("++++工作室学生rmName:" + rmName);
        RoomStudent room = new RoomStudent();
        room.setRmName(rmName);
        room.setRmStudent(rmStudent);
        room.setOpenid(openid);
        room.setRmTeacher(rmTeacher);
        room.setRmPhone(rmPhone);
        room.setRmPlace(rmPlace);
        room.setStat(stat);
        int insertRoom = roomStudentMapper.insertRoom(room);
        System.out.println("insertRoom:"+insertRoom);
        map.put("insertRoom",insertRoom);
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 修改申请加入的工作室学生的stat=1使其审核成功
	 * @param request 修改的学生
	 * @return 成功返回大于等于1，失败返回小于等于0的整数
	 */
	@ResponseBody
	@RequestMapping("/UpdateStudent")
    public String UpdateStudent(HttpServletRequest request) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //获取页面传过来的值
        String rmName = request.getParameter("rmName");
        String rmStudent = request.getParameter("rmStudent");
        String openid = request.getParameter("openid");
        String stat = request.getParameter("stat");;
        System.out.println("++++工作室学生openid:" + request.getParameter("openid"));
        System.out.println("++++工作室学生rmName:" + rmName);
        RoomStudent room = new RoomStudent();
        room.setRmName(rmName);
        room.setRmStudent(rmStudent);
        room.setOpenid(openid);
        room.setStat(Integer.valueOf(stat));
        int updateRoom = roomStudentMapper.updateStudent(room);
        System.out.println("updateRoom:"+updateRoom);
        map.put("updateRoom",updateRoom);
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 取消等待审核
	 * @param request 新的学生
	 * @return 成功返回大于等于1，失败返回小于等于0的整数
	 */
	@ResponseBody
	@RequestMapping("/updateRoomStudentStat")
    public String updateRoomStudentStat(HttpServletRequest request) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //获取页面传过来的值
        String rmName = request.getParameter("rmName");  
        String openid = request.getParameter("openid");
        String stat = request.getParameter("stat");
        System.out.println("194(学生状态rmName)："+rmName);
        System.out.println("194(学生状态openid)："+openid);
        System.out.println("194(学生状态)："+stat);
        if(Integer.valueOf(stat) == 0) {
        	RoomStudent room = new RoomStudent();
        	room.setRmName(rmName);
            room.setOpenid(openid);
            int deleteStat = roomStudentMapper.deleteStudent(room);
            System.out.println("deleteStat:"+deleteStat);
            map.put("deleteStat",deleteStat);
            //返回map的json键值
            return JSONObject.toJSON(map).toString();
        }else {
        	System.out.println("修改失败");
        	return null;
        }
    }
	
}
