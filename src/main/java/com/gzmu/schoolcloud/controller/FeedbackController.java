package com.gzmu.schoolcloud.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzmu.schoolcloud.mapper.FeedbackMapper;
import com.gzmu.schoolcloud.model.Feedback;

@Controller
public class FeedbackController {

	@Autowired 
	private FeedbackMapper feedbackMapper;
	
	/**
     * 添加反馈信息
     * @param request 接收页面传递的反馈信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertFeedback", produces = "text/html;charset=UTF-8")
    public String insertFeedback(HttpServletRequest request){
        //new一个map对象
        Map<String, Integer> map = new HashMap<String, Integer>();
        //new一个Repair对象
        Feedback feedback = new Feedback();
        //new一个ObjectMapper对象进行格式处理
        ObjectMapper mapper = new ObjectMapper();
        //接收页面传递过来的openid
        String openid = request.getParameter("openid");
        String feName = request.getParameter("feName");
        String fePhone = request.getParameter("fePhone");
        String feText = request.getParameter("feText");
        System.out.println("反馈人:"+feName);
        //将接收的信息添加到repair类对应的变量中
        feedback.setOpenid(openid);
        feedback.setFeName(feName);
        feedback.setFePhone(fePhone);
        feedback.setFeText(feText);
        
        //成功添加到数据库中返回大于等于1的整数，失败返回小于等于0 的整数
        int res = feedbackMapper.insertFeedback(feedback);
        System.out.println("返回值："+res);
        try {
        	//失败将值小于等于0的整数以json的格式传递到页面
            map.put("res",res);
            mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return JSONObject.toJSON(map).toString();
    }
}
