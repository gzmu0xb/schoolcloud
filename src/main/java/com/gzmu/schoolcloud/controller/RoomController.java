package com.gzmu.schoolcloud.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzmu.schoolcloud.mapper.RoomMapper;
import com.gzmu.schoolcloud.mapper.RoomStudentMapper;
import com.gzmu.schoolcloud.model.Room;
import com.gzmu.schoolcloud.model.RoomStudent;
@Controller
public class RoomController {

	@Autowired
	private RoomMapper roomMapper;
	@Autowired
	private RoomStudentMapper roomStudentMapper;
	
	/**
	 * 查询所有工作室信息
	 * @param request
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/selectRoom", produces = "text/html;charset=UTF-8")
    public String Room() {
    	//创建一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //查询roomMapper的数据
        List<Room> list = roomMapper.findAll();
        //把值添加到map中
        map.put("list",list);
        //以json的格式返回值到页面中
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 查询所有工作室名字
	 * @param request
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/selectAllRoomName", produces = "text/html;charset=UTF-8")
    public String selectAllRoomName(HttpServletRequest request) {
    	//创建一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //查询roomMapper的数据
        List<String> allRoomName = roomMapper.selectAllRoomName();
        for (String name : allRoomName) {
        	System.out.print(name);
		}
        //把值添加到map中
        map.put("allRoomName",allRoomName);
        //以json的格式返回值到页面中
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 根据工作室名字查询工作室信息
	 * @param request
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/selectRoomByName", produces = "text/html;charset=UTF-8")
    public String selectRoomByName(HttpServletRequest request) {
    	//创建一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //接收页面传递过来要查询工作室的名称
        String roomName = request.getParameter("rmName");
        //查询roomMapper的数据
        Room room = roomMapper.findRoomByName(roomName);
        //把值添加到map中
        map.put("room",room);
        //以json的格式返回值到页面中
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 根据工作室名字模糊查询工作室信息
	 * @param request
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/selectRoomByNameLike", produces = "text/html;charset=UTF-8")
    public String selectRoomByNameLike(HttpServletRequest request) {
    	//创建一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //接收页面传递过来要查询工作室的名称
        String roomName = request.getParameter("roomName");
        //查询roomMapper的数据
        List<Room> list = roomMapper.dimRoomName(roomName);
        //把值添加到map中
        map.put("list",list);
        //以json的格式返回值到页面中
        return JSONObject.toJSON(map).toString();
    }
	
	/**
	 * 根据工作室oenid查询工作室信息
	 * @param request
	 * @return
	 */
	@ResponseBody
    @RequestMapping(value = "/selectRoomByOpenid", produces = "text/html;charset=UTF-8")
    public String selectRoomByOpenid(HttpServletRequest request) {
    	//创建一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //接收页面传递过来要查询工作室的名称
        String openid = request.getParameter("openid");
        //查询roomMapper的数据
        List<Room> rooms = roomMapper.findRoomByOpenid(openid);
        //把值添加到map中
        map.put("rooms",rooms);
        //以json的格式返回值到页面中
        return JSONObject.toJSON(map).toString();
    }
	
	/**
     *  添加报修信息
     * @param request 接收页面传递的保修信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertRoom", produces = "text/html;charset=UTF-8")
    public String insertRoom(HttpServletRequest request){
        //new一个map对象
        Map<String, Integer> map = new HashMap<String, Integer>();
        //new一个Repair对象
        Room room = new Room();
        //new一个ObjectMapper对象进行格式处理
        ObjectMapper mapper = new ObjectMapper();
        //接收页面传递过来的openid
        String openid = request.getParameter("openid");
        String branch = request.getParameter("branch");
        String roomName = request.getParameter("roomName");
        String roomType = request.getParameter("roomType");
        String roomTime = request.getParameter("roomTime");
        String roomPlace = request.getParameter("roomPlace");
        String roomLongitude = request.getParameter("roomLongitude");
        String roomLatitude = request.getParameter("roomLatitude");
        String roomCampus = request.getParameter("roomCampus");
        String roomTeacher = request.getParameter("roomTeacher");
        String roomEmail = request.getParameter("roomEmail");
        String roomPhone = request.getParameter("roomPhone");
        String roomImg = request.getParameter("roomImg");
        System.out.println("工作室位置:"+roomPlace);
        System.out.println("工作室经度:"+roomLongitude);
        if(roomLatitude == "undefined" || roomLatitude == null) {
        	roomLatitude = "0";
        }
        if(roomLongitude == "undefined" || roomLongitude == null ) {
        	roomLongitude = "0";
        }
        System.out.println("工作室经度:"+roomLongitude);
        //将接收的信息添加到repair类对应的变量中
        room.setOpenid(openid);
        room.setBranch(branch);
        room.setRoomName(roomName);
        room.setRoomType(roomType);
        room.setRoomTime(roomTime);
        room.setRoomPlace(roomPlace);
        room.setRoomLongitude(roomLongitude);
        room.setRoomLatitude(roomLatitude);
        room.setRoomCampus(roomCampus);
        room.setRoomTeacher(roomTeacher);
        room.setRoomEmail(roomEmail);
        room.setRoomPhone(roomPhone);
        room.setRoomImg(roomImg); 
        //成功添加到数据库中返回大于等于1的整数，失败返回小于等于0 的整数
        int res = roomMapper.insertRoom(room);
        System.out.println("159res："+res);
        //创建工作室成功在工作室成员表room_student中自动生成信息
        if(res >= 1) {
        	RoomStudent student = new RoomStudent();	
        	student.setRmName(roomName);
        	student.setRmStudent(roomTeacher);
        	student.setOpenid(openid);
        	student.setRmTeacher(roomTeacher);
        	student.setRmPhone(roomPhone);
        	student.setRmPlace(roomPlace);
        	student.setStat(1);
            int insertRoom = roomStudentMapper.insertRoom(student);
            System.out.println("170insertRoom:"+insertRoom);
        }
        try {
        	//失败将值小于等于0的整数以json的格式传递到页面
            map.put("res",res);
            mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return JSONObject.toJSON(map).toString();
    }
}
