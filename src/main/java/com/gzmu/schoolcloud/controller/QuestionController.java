package com.gzmu.schoolcloud.controller;

import com.gzmu.schoolcloud.mapper.QuestionMapper;
import com.gzmu.schoolcloud.model.Question;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class QuestionController {

    @Autowired
    private QuestionMapper questionMapper;

    /**
     * 获取题库数据
     * @param testId 试卷的类型
     * @return
     */

    @RequestMapping("findAllQue")
    @ResponseBody
    public List findAllque(String testId){
        //获取题目及其它数据
        List<Map<String,Object>> allQuestion = questionMapper.findAllQuestionByTestId(testId);
        //获取选项数据
        List<Map<String,Object>> allOption = questionMapper.findAllOptionByTestId(testId);
        //把选项数据加到题目里面
        for(int i = 0;i<allQuestion.size();i++){
            Map<String, Object> stringObjectMap = allQuestion.get(i);
            Map<String, Object> stringObjectMap1 = allOption.get(i);
            stringObjectMap.put("option",stringObjectMap1);
        }
        System.out.println("题目数据："+allQuestion);
        System.out.println("选项数据："+allOption);
        return allQuestion;
    }
}
