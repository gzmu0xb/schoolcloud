package com.gzmu.schoolcloud.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gzmu.schoolcloud.mapper.RepairMapper;
import com.gzmu.schoolcloud.model.*;
import com.gzmu.schoolcloud.wx.msg.util.SMessage;
import com.gzmu.schoolcloud.wx.util.ParamesAPI;
import com.gzmu.schoolcloud.wx.util.WeixinUtil;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.gzmu.schoolcloud.constant.Constant.*;
import static com.gzmu.schoolcloud.vo.CommonUtil.getOpenid;

@Controller
public class RepairController {
	
	//对repairMapper层的接口进行调用
    @Autowired
    private RepairMapper repairMapper;

    /**
     * 获取openid
     * @param code 接收传递过来code
     * @return 成功返回map转化成json字符串的Teacher对象，失败返回null
     */
    @ResponseBody
    @RequestMapping(value = "/getRepairOpenid", produces = "text/html;charset=UTF-8")
    public String getRepairOpenid(String code){
        System.out.println("+++code: " + code);
        String content = null;
        //new一个map对象
        Map<String, Teacher> map = new HashMap<String, Teacher>();
        //new一个ObjectMapper对象进行格式处理
        ObjectMapper mapper = new ObjectMapper();
        //new一个Teacher对象
        Teacher teacher = new Teacher();
        //设置openid
        String openid = getOpenid(code,BX_ID,BX_SECRET);
        //判断openid不为空：isBlank 是在 isEmpty 的基础上进行了为空（字符串都为空格、制表符、tab 的情况）的判断
        if(!StringUtils.isBlank(openid)){
        	//对openid字符串进行替换"\"字符替换为空
            openid = openid.replace("\"", "").trim();
            //根据老师表中的openid查询老师表里面的所有信息0
            teacher = repairMapper.findUerByOpenId(openid);
            //判断查询的教师表是否为空
            if (teacher == null){
            	//为空，重新newTeacher对象
                teacher = new Teacher();
                //对教师表添加一个openid
                teacher.setOpenid(openid);
                //根据openid对教师表添加信息
                repairMapper.insertTeacher(teacher);
                //添加教师id
                teacher.setId(0);
            }
        }
        //异常处理
        try {
        	//无异常则添加当前教师信息在map中
            map.put("teacher", teacher);
            //writeValueAsString把java对象map转化成json字符串
            content = mapper.writeValueAsString(map);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //成功返回map转化成json字符串的Teacher对象，失败返回null
        return content;
    }
    
    /**
     * 学院信息
     * @param request获取请求参数， 出现中文乱码的解决办法produces
     * @return 成功以json格式返回学院信息到页面
     */
    @ResponseBody
    @RequestMapping(value = "/branchList", produces = "text/html;charset=UTF-8")
    public String branchList(HttpServletRequest request) {
    	//创建一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //接收repairMapper层传递过来的数据
        List<String> list = repairMapper.branchList();
        //把值添加到map中
        map.put("list",list);
        //以json的格式返回值到页面中
        return JSONObject.toJSON(map).toString();
    }

    /**
	 * 修改教师信息
	* @param request 接收页面传到过来的教师信息
	* @return 成功返回修改后的教师信息，失败则返回null
	*/
    @ResponseBody
    @RequestMapping(value = "/updateTeacher", produces = "text/html;charset=UTF-8")
    public String updateTeacher(HttpServletRequest request){
        String content = null;
        //new一个hashMap对象
        Map<String, Integer> map = new HashMap<String, Integer>();
        //new一个teacher对象
        Teacher teacher = new Teacher();
        //new一个ObjectMapper对象进行格式处理
        ObjectMapper mapper = new ObjectMapper();        
        //接收页面传递过来要修改的信息
        String wxid = request.getParameter("wxid");
        String openid = request.getParameter("openid");
        String nickName = request.getParameter("nickName");
        String name = request.getParameter("name");
        String gender = request.getParameter("gender");
        String avatarUrl = request.getParameter("avatarUrl");
        String phone = request.getParameter("phone");
        String branch = request.getParameter("branch");
        String title = request.getParameter("title");
        String role = request.getParameter("role");    
        //把页面传递过来的teacher信息用teache类来接收
        teacher.setWxid(wxid);
        teacher.setOpenid(openid);
        teacher.setNickName(nickName);
        teacher.setName(name);
        teacher.setGender(gender);
        teacher.setAvatarUrl(avatarUrl);
        teacher.setPhone(phone);
        teacher.setBranch(branch);
        teacher.setTitle(title);
        teacher.setRole(Integer.valueOf(role));
        System.out.println("+++updateTeacher openid:" + openid + " role:" + role);
        //接收修改后的信息，成功返回大于等于1的整数，失败返回小于等于0的整数
        int res = repairMapper.updateTeacher(teacher);
        //异常处理部分
        try {
        	//无异常map接收res
            map.put("res",res);
            //writeValueAsString把java对象map转化成json字符串
            content = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //成功返回大于等于1的整数，失败返回小于等于0的整数json字符
        return content;
    }

    /**
             * 对保修楼层进行添加
     * @param request 接收页面返回过来的build教学，和floor楼层
     * @return 返回map的json键值
     */
    @ResponseBody
    @RequestMapping(value = "/roomList", produces = "text/json;charset=UTF-8")
    public String roomList(HttpServletRequest request) {
    	//接收页面传递过来的保修教学楼build的信息
        String buildStr = request.getParameter("build");
        //接收页面传递过来的保修楼层floor的信息
        String floorStr = request.getParameter("floor");
        
        //查询bx_build表中所有Build教学楼，不重复
        List<String> builds = repairMapper.selectAllBuild();
        //如果页面传递过来的值为null或者是“教学楼”
        if (buildStr == null || buildStr.equals("教学楼")) {
        	//则取到builds集合里面build字段的第一个数组信息
            buildStr = builds.get(0);
        }
        
        //根据buildStr查询bx_build表中所有floor字段楼层的所有信息，不重复
        List<String> floors = repairMapper.selectAllFloor(buildStr);
        //如果页面传递过来的值为null或者是“楼层”
        if (floorStr == null || floorStr.equals("楼层")) {
        	//则取到builds集合里面floor字段的第一个数组信息
            floorStr = floors.get(0);
        }
        System.out.println("+++buildStr:" + buildStr + " floor:" + floorStr);
        //new一个build对象
        Build build = new Build();
        //把从数据库中查询的buildSt所有教学楼，与对应所有楼层的floor信息添加到build类中
        build.setBuild(buildStr);
        build.setFloor(floorStr);
        
        //并根据build对象中的值进行对应教师的查询
        List<String> rooms = repairMapper.selectAllRoom(build);
        //new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //把查询教学楼，对应楼层，对应教师的信息添加到map中
        map.put("builds",builds);
        map.put("floors",floors);
        map.put("rooms",rooms);
        //传递json格式的map到页面
        return JSONObject.toJSON(map).toString();
    }

    /**
     * 图片的上传
     * @param openid 上传者的openid
     * @param file 上传保修的图片信息
     * @return 返回map的json键值
     */
    @ResponseBody
    @PostMapping("/uploadimg")
    public String uploadimg(@RequestParam("openid") String openid, @RequestParam("file") MultipartFile file) {
    	//new一个map对象
        Map<String, Object> map = new HashMap<String, Object>();
        //如果上传的图片是null
        if (file.isEmpty()) {
        	//对map中result键添加值为false
            map.put("result",false);
            //对map中url键添加值为空
            map.put("url","");
            //并向页面返回
            return JSONObject.toJSON(map).toString();
        }
        System.out.println("++++upload openid:" + openid);
        //得到上传时的文件名
        String fileName = file.getOriginalFilename();
        //利用UUID创建一个随机且不重复的，名字
        String newName = UUID.randomUUID().toString();
        //获取系统文件分隔符，取出"."以下的所有信息（取后缀名）
        String type = fileName.substring(fileName.lastIndexOf(".") + 1);
        //对上传的文件名取名（去掉后缀名）
        String name = fileName.substring(0,fileName.lastIndexOf("."));
        //将系统格式的时间转换为文本（字符）格式
        String nowDate = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String timeStr = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date());
        try {
        	//设置存储图片路径
            String path = "E:/schoolcloud/" ;
            File epath=new File(path);
            //getAbsolutePath取得绝对路径
            File uploadPath = new File(epath.getAbsolutePath(),"/static/upfile/"  + nowDate + "/") ;
            //System.out.println("+++uploadPath:" + uploadPath.getPath());
            //文件夹路径不存在
            if (!uploadPath.exists() && !uploadPath.isDirectory()) {
                System.out.println("+++mrdir:" + uploadPath);
                // 创建此抽象路径名指定的目录，包括所有必需但不存在的父目录。可以创建多级目录
                uploadPath.mkdirs();
            }
            //文件路径
            String nPath = uploadPath +"/"+ newName + "." + type;
            //创建一个newFile的文件名，对路径为nPath的目录文件进行操作
            File newFile = new File(nPath);
            //transferto()方法 把内存中图片写入磁盘
            file.transferTo(newFile);
            
            //定义文件的url路径
            String fileUrl = "upfile/" + nowDate + "/" + newName + "." + type;
            //new一个图片文件ImgFile的对象
            ImgFile imgFile = new ImgFile();
            //添加openid
            imgFile.setOpenid(openid);
            //添加文件名字
            imgFile.setOldName(name);
            //添加文件路径名
            imgFile.setUrlName(fileUrl);
            //是否可以
            imgFile.setIsEnble(1L);
            //添加文件上传时间
            imgFile.setTime(timeStr);
            //判断文件是否上传成功
            if (repairMapper.insertImgFile(imgFile)==1){
                System.out.println("\r\n"+name + " upload success");
                //成功设置result键中的值为true
                map.put("result",true);
                //成功给map键url添加值文件路径
                map.put("url",fileUrl);
            }else{
                System.out.println(name + " upload fail");
                //失败设置result键中的值为false
                map.put("result",false);
                //失败给map键url添加值为空路径
                map.put("url","");
            }
        } catch (IOException e) {
        	//异常则设置失败
            map.put("result",false);
            //失败给map键url添加值为空路径
            map.put("url","");
            System.out.println(e.toString());
        }
        //返回map的json键值
        return JSONObject.toJSON(map).toString();
    }

    /**
     * 添加报修信息
     * @param request 接收页面传递的保修信息
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/insertRepair", produces = "text/html;charset=UTF-8")
    public String insertRepair(HttpServletRequest request){
    	//定义一个content的String变量
        String content = null;
        //new一个map对象
        Map<String, Integer> map = new HashMap<String, Integer>();
        //new一个Repair对象
        Repair repair = new Repair();
        //new一个ObjectMapper对象进行格式处理
        ObjectMapper mapper = new ObjectMapper();
        //将系统的时间转换为文本信息
        String timeStr = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date());
        //接收页面传递过来的openid
        String openid = request.getParameter("openid");
        //接收页面传递过来的要报修的教学楼的信息
        String build = request.getParameter("build");
        //接收页面传递过来的要报修的楼层信息
        String floor = request.getParameter("floor");
        //接收页面传递过来的要报修的教室信息
        String room = request.getParameter("room");
        //接收页面传递过来的要报修的设备信息
        String facility = request.getParameter("facility");
        //接收页面传递过来的要报修的人的微信id
        String tid = request.getParameter("tid");
        //接收页面传递过来的要报修的人的微信名字
        String tname = request.getParameter("tname");
        //接收页面传递过来的要报修的设备坏的地方（描述）
        String tfanlt = request.getParameter("tfanlt");
        //接收页面传递过来的要报修的的图片信息
        String timg = request.getParameter("timg");
        String latitude = request.getParameter("latitude");
        String longitude = request.getParameter("longitude");
        System.out.println("经度:"+longitude);
        //将接收的信息添加到repair类对应的变量中
        repair.setOpenid(openid);
        repair.setBuild(build);
        repair.setFloor(floor);
        repair.setRoom(room);
        repair.setFacility(facility);
        repair.setTid(tid);
        repair.setTname(tname);
        repair.setTfanlt(tfanlt);
        repair.setTimg(timg);
        repair.setTtime(timeStr);
        repair.setLatitude(latitude);
        repair.setLongitude(longitude);
        //成功添加到数据库中返回大于等于1的整数，失败返回小于等于0 的整数
        int res = repairMapper.insertRepair(repair);
        //添加成功res为1
        if ((res==1) && (!room.isEmpty()) && (!tname.isEmpty())){
        	//定义一个str变量处理信息传值到公众号
            String str = "教学楼：" + build + "\r\n教室号：" + room + "\r\n故障设备：" + facility +
                    "\r\n故障描述：" + tfanlt + "\r\n报修老师：" + tname;
            String PostData = SMessage.STextMsg("yxb422576903", ParamesAPI.Oneself, ParamesAPI.Artisan,
                    ParamesAPI.AgentId, str);
            String access_token = WeixinUtil.getAccessTokenString(ParamesAPI.corpId, ParamesAPI.secret);
            int result = WeixinUtil.PostMessage(access_token, "POST", SMessage.POST_URL, PostData);
            System.out.println("+++insertRepair PostData:\r\n"+PostData + " \r\nresult:" + result);
        }
        try {
        	//失败将值小于等于0的整数以json的格式传递到页面
            map.put("res",res);
            content = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //成功返回大于等于1的整数，失败返回小于等于0 的整数
        return content;
    }

    /**
     * 管理员对上传报修的设备进行维修
     * @param request 接收维修员的信息
     * @return //成功返回大于等于1的整数，失败返回小于等于0 的整数以json格式传递到页面
     */
    @ResponseBody
    @RequestMapping(value = "/updateRepair", produces = "text/html;charset=UTF-8")
    public String updateRepair(HttpServletRequest request){
    	//定义一个content变量
        String content = null;
        //new一个map对象
        Map<String, Integer> map = new HashMap<String, Integer>();
        //new一个Repair对象
        Repair repair = new Repair();
        //new一个ObjectMapper对象进行格式处理
        ObjectMapper mapper = new ObjectMapper();
        //把当前系统时间转换成文本信息
        String wtime = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date());
        //接收页面传递过来的维修员信息
        //id表示报修的id，wid表示维修员的信息
        String id = request.getParameter("id");
        String wid = request.getParameter("wid");
        String wname = request.getParameter("wname");
        String wimg = request.getParameter("wimg");
        String wfanlt = request.getParameter("wfanlt");
        //把接收过来的维修员信息添加到repair中
        //返回值为包装类型的id(repair的Id)
        repair.setId(Integer.valueOf(id));
        repair.setWid(wid);
        repair.setWname(wname);
        repair.setWimg(wimg);
        repair.setWtime(wtime);
        repair.setWfanlt(wfanlt);
        //state为1代表维修员，0表示教师或报修的同学
        repair.setState(1);
        //成功返回大于等于1的整数，失败返回小于等于0 的整数
        int res = repairMapper.updateRepair(repair);
        //根据报修id查询bx_repair表中对应的所有信息
        repair = repairMapper.selectRepairById(id);
        //成功默认为1
        if (res==10){
        	//向微信公众号返回信息
            String msgStr = "老师您在" + repair.getBuild() + repair.getRoom() + "报修的" + repair.getFacility()
                    + "设备已检修,详细请进入小程序查看\r\n维修员：" + wname;
//            String PostData = SMessage.STextMsg(repair.getTid(), ParamesAPI.Oneself, ParamesAPI.Oneself,
//                    ParamesAPI.AgentId, msgStr);
            String access_token = WeixinUtil.getAccessTokenString(ParamesAPI.corpId, ParamesAPI.secret);
//           int result =  WeixinUtil.PostMessage(access_token, "POST", SMessage.POST_URL, PostData);
//           System.out.println("+++updateRepair PostData:\r\n"+PostData + " \r\nresult:" + result);
        }
        try {
        	//失败返回小于等于0 的整数以json格式传递到页面
            map.put("res",res);
            content = mapper.writeValueAsString(map);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        //成功返回大于等于1的整数，失败返回小于等于0 的整数以json格式传递到页面
        return content;
    }

    /**
     * 查询报修信息
     * @param request 接收用户的openid和state（是否是维修员）
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectRepair", produces = "text/html;charset=UTF-8")
    public String selectRepair(HttpServletRequest request) {
    	//接收当前用户的openid
        String openid = request.getParameter("openid");
        //接收当前用户的state（0普通用户(未完成)，1维修员（已完成））
        String stat = request.getParameter("state");
        //接收页面传到查询是数量
        String nu = request.getParameter("num");
        Integer num = Integer.valueOf(nu);
        Integer state = Integer.valueOf(stat);
        //new一个Repair对象
        Repair repair = new Repair();
        //把从页面接收来的信息添加到repair中
//        repair.setOpenid(openid);
//        repair.setState(Integer.valueOf(state));
        
        //new一个HashMap对象
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("+++selectRepair openid:" + openid + " state:" + state + " num:" + num);
        //根据当前用户的openid和state查询repair的对应信息
        List<Repair> list = repairMapper.selectRepair(openid,state,num);
        //并添加到map中
        map.put("list",list);
        //将查询出来的值传到到页面中json格式
        return JSONObject.toJSON(map).toString();
    }
    
    /**
     * 查询上报的维修列表总数
     */
    @ResponseBody
    @RequestMapping(value = "/selectRepairLength", produces = "text/html;charset=UTF-8")
    public String selectRepairLength(HttpServletRequest request) {
    	//接收当前用户的openid
        String openid = request.getParameter("openid");
        //接收当前用户的state（0普通用户(未完成)，1维修员（已完成））
        String state = request.getParameter("state");
        //new一个Repair对象
        Repair repair = new Repair();
        //把从页面接收来的信息添加到repair中
        repair.setOpenid(openid);
        repair.setState(Integer.valueOf(state));
        
        //new一个HashMap对象
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("+++selectRepair openid:" + openid + " state:" + state );
        //根据当前用户的openid和state查询repair的对应信息
        int myRepairLength = repairMapper.selectRepairLength(repair);
        //并添加到map中
        map.put("myRepairLength",myRepairLength);
        //将查询出来的值传到到页面中json格式
        return JSONObject.toJSON(map).toString();
    }
    

    /**
     * 根据上传报修的id查询对应的报修信息
     * @param request 接收页面返回的报修id
     * @return 成功返回查询的报修信息，失败返回null
     */
    @ResponseBody
    @RequestMapping(value = "/selectRepairById", produces = "text/html;charset=UTF-8")
    public String selectRepairById(HttpServletRequest request) {
    	//接收页面传递过来的报修id
        String id = request.getParameter("id");
        //new一个HashMap
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("+++id:" + id);
        //根据报修id，查询对应的报修信息
        Repair repair = repairMapper.selectRepairById(id);
        System.out.println("496：repair:"+repair);
        //并将值添加到map中
        map.put("repair",repair);
        //将map中对应的repair信息传递到页面
        return JSONObject.toJSON(map).toString();
    }

    /**
            * 根据维修员id
     * @param request 接收页面传递过来的维修员id和state
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/selectRepairByWidState", produces = "text/html;charset=UTF-8")
    public String selectRepairByWidState(HttpServletRequest request) {
    	//new一个HashMap
        Map<String, Object> map = new HashMap<String, Object>();
        //接收页面传递过来发维修员id和state
        String wid = request.getParameter("wid");
        String state = request.getParameter("state");
        String nu = request.getParameter("num");
        Integer num = Integer .valueOf(nu);
        System.out.println("+++wid:" + wid + " state:" + state + " num:" + nu);
        //定义一个list集合接收Repair
        List<Repair> list;
        //如果传递过来的state的值为1
        if (state.equals("1")){
        	//new一个repair对象接收wd和state
            Repair repair = new Repair();
            repair.setWid(wid);
            repair.setState(Integer.valueOf(state));
            //根据state和wid查询所有的bx_repair报修表中的不是维修员的所有信息
            list = repairMapper.selectRepairByWidState(repair);
        }else{
        	//state=0，根据state查询所有的bx_repair报修表中的不是维修员的所有信息
            list = repairMapper.selectRepairByState(state,num);
        }
        //并把值加到list集合中
        map.put("list",list);
        //以json格式传递到页面
        return JSONObject.toJSON(map).toString();
    }

    /**
     * 查询上报的待维修列表总数
     */
    @ResponseBody
    @RequestMapping(value = "/selectRepairStateLength", produces = "text/html;charset=UTF-8")
    public String selectRepairStateLength(HttpServletRequest request) {
        //接收当前用户的state（0普通用户(未完成)，1维修员（已完成））
        String state = request.getParameter("state");
        //new一个HashMap对象
        Map<String, Object> map = new HashMap<String, Object>();
        System.out.println("+++selectRepair state:" + state );
        //根据当前用户的openid和state查询repair的对应信息
        int stateLength = repairMapper.selectRepairStateLength(state);
        //并添加到map中
        map.put("stateLength",stateLength);
        //将查询出来的值传到到页面中json格式
        return JSONObject.toJSON(map).toString();
    }
    
    /**
     * 根据当前的openid查询所有的state=0的总数和state等于1的总数
     * @param request 获取当前的openid
     * @return 
     */
    @ResponseBody
    @RequestMapping(value = "/totalRepairState", produces = "text/html;charset=UTF-8")
    public String totalRepairState(HttpServletRequest request) {
    	//new一个HashMap
        Map<String, Object> map = new HashMap<String, Object>();
        //接收当前用户的openid
        String openid = request.getParameter("openid");
        System.out.println("+++totalRepairState openid:" + openid);
        //根据当前用户openid查询所有的state=0的总数和state等于1的总数（wait表示state=0的总数，finish表示state=1的所有总数）
        //及查询自己上报了多少设备，以及自己维修了多少设备
        Status status= repairMapper.totalRepairState(openid);
        //根据当前用户的openid查询对应用户的所有信息（bx_user表）
        Teacher teacher = repairMapper.findUerByOpenId(openid);
        //将查询出来的数据添加到map中
        map.put("status",status);
        map.put("user",teacher);
        //将查询出来的值传递到页面中
        return JSONObject.toJSON(map).toString();
    }
    
    /**
             * 查询所有的用户信息
     * @return 成功返回查询出来的数据，失败返回null
     */
    @ResponseBody
    @RequestMapping(value = "/selectAllBxUser", produces = "text/html;charset=UTF-8")
    public String selectAllBxUser() {
        Map<String, Object> map = new HashMap<String, Object>();
        //查询bx_user表中的所有用户信息
        List<User> list = repairMapper.selectAll();
        System.out.println(list);
        map.put("list",list);
        //将查询出来的值传递到页面中
        return JSONObject.toJSON(map).toString();
    }
}
