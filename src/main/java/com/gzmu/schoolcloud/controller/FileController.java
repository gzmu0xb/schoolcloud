package com.gzmu.schoolcloud.controller;

import com.gzmu.schoolcloud.mapper.FileMapper;
import com.gzmu.schoolcloud.model.File;
import com.alibaba.fastjson.JSONObject;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


@RestController
public class FileController {

    @Autowired
    private FileMapper fileMapper;

    @RequestMapping("/upfile")
    public String upfile(HttpServletRequest request, Model model) {
        String uid = request.getParameter("uid");

        System.out.println("++++upfile uid:" + uid );
        if (uid != null)
            model.addAttribute("uid", uid);
        return "/app/upfile";
    }

    @ResponseBody
    @PostMapping("/uploadFile")
    public String upload(@RequestParam("file") MultipartFile file,HttpServletRequest request) {
        if (file.isEmpty()) {
            return "上传失败，请选择文件";
        }
        String openid = request.getParameter("openid");
        String roomName = request.getParameter("roomName");
        String branch = request.getParameter("branch");
        String path = "E:/schoolcloud/" ;
        //获取文件名
        String fileName = request.getParameter("fileName");
        System.out.println("上传文件名："+fileName);
        System.out.println("55++++upload branch:" + branch);
        if (fileName==null)
        	fileName = file.getOriginalFilename();
        //随机生成文件名，避免重复
        String newName = UUID.randomUUID().toString();
        // 获取系统文件分隔符
        String type = fileName.substring(fileName.lastIndexOf(".") + 1);
        String name = fileName.substring(0,fileName.lastIndexOf("."));
        System.out.println("第二个上传文件名："+fileName);
        //格式化日期
        String nowDate = new SimpleDateFormat("YYYY-MM-dd").format(new Date());
        String timeStr = nowDate;
        try {
            //文件路径
            java.io.File epath = new java.io.File(path);
            //System.out.println("根目录:" + path.getAbsolutePath());
            java.io.File uploadPath = new java.io.File(epath.getAbsolutePath(),"static/upfile/" + nowDate + "/") ;
            //文件夹路径不存在
            System.out.println("文件存储路径："+uploadPath);
            if (!uploadPath.exists() && !uploadPath.isDirectory()) {
                System.out.println("创建路径:" + uploadPath);
                uploadPath.mkdirs();
                System.out.println("文件存储路径："+uploadPath);
            }

            String nPath = uploadPath +"/"+ newName + "." + type;
            System.out.println("npath"+nPath);
            java.io.File newFile = new java.io.File(nPath);
            System.out.println("newFile"+newFile);
            file.transferTo(newFile);
            //System.out.println("pages:" + pages);
            //把文件信息存储到数据库
            String fileUrl = "/upfile/" + nowDate + "/" + newName + "." + type;
            File f = new File();
            f.setOpenid(openid);
            f.setType(type);
            f.setOldName(name);
            f.setUrlName(fileUrl);
            f.setTime(timeStr);
            f.setDowncounts(0);
            f.setRoomName(roomName);
            f.setBranch(branch);
            Map<String, Object> map = new HashMap<String, Object>();
            int reValue = fileMapper.insertFile(f);

            //判断数据存储是否成功
            if (reValue==1){
                System.out.println(fileName + " upload success");
                map.put("result",true);
            }else{
                System.out.println(fileName + " upload fail");
                map.put("result",false);
            }
            map.put("name",fileName);
            map.put("path",fileUrl);
            return JSONObject.toJSON(map).toString();
        } catch (IOException e) {
            System.out.println(e.toString());
        }
        return "upload fail";
    }


    //查出所有文件
    @RequestMapping("/findAllFile")
    public List<File> findAllJSON( Model model) {
        //查询所有的文件信息
        List<File> userFiles = fileMapper.findAll();
        //赋值
        model.addAttribute("files", userFiles);
        System.out.println(userFiles);
        return userFiles;
    }
    
    //根据openid查出自己发布的文件文件
    @RequestMapping("/findAllMyFile")
    public String findAllMyFile(HttpServletRequest request) {
    	String openid = request.getParameter("openid");
    	String nu = request.getParameter("myNum");		//接收一定的文件数量
    	System.out.println("148Mynum:"+nu);
    	System.out.println("openid:"+openid);
       	Integer num = Integer.valueOf(nu);
        //查询所有的文件信息
        List<File> userFiles = fileMapper.findMyAllFile(openid,num);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("userFiles",userFiles);
        System.out.println(userFiles);
        return JSONObject.toJSON(map).toString();
    }
    
    //根据roomName查出所有文件的长度
    @RequestMapping("/findMyFileLength")
    public String findMyFileLength(HttpServletRequest request) {
    	String openid = request.getParameter("openid");
    	System.out.println("166roomName:"+openid);
        //查询所有的文件长度根据openid
        int myFileLength = fileMapper.findMyFileLength(openid);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("myFileLength",myFileLength);
        System.out.println(myFileLength);
        return JSONObject.toJSON(map).toString();
    }
    
    //根据roomName查出自己发布的文件文件
    @RequestMapping("/findAllRoomFile")
    public String findAllRoomFile(HttpServletRequest request) {
    	String roomName = request.getParameter("roomName");
    	String nu = request.getParameter("num");		//接收一定的文件数量
    	System.out.println("148roomName:"+roomName);
    	System.out.println("148num:"+nu);
    	Integer num = Integer.valueOf(nu);
        //查询所有的文件信息
        List<File> roomNameFiles = fileMapper.findRoomAllFile(roomName, num);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("roomNameFiles",roomNameFiles);
        System.out.println(roomNameFiles);
        return JSONObject.toJSON(map).toString();
    }
    
    //根据roomName查出所有文件的长度
    @RequestMapping("/findAllFileLenth")
    public String findAllFileLenth(HttpServletRequest request) {
    	String roomName = request.getParameter("roomName");
    	System.out.println("166roomName:"+roomName);
        //查询所有的文件长度根据roomName
        int fileLength = fileMapper.findFileLenth(roomName);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("fileLength",fileLength);
        System.out.println(fileLength);
        return JSONObject.toJSON(map).toString();
    }

    //下载文件
    @RequestMapping("downloadFile")
    public void download(String openStyle, String id, HttpServletResponse response) throws IOException {
        //获取打开方式
        openStyle = openStyle == null ? "attachment" : openStyle;
        //获取文件信息
        File file = fileMapper.findById(id);
        //点击下载链接更新下载次数
        if ("attachment".equals(openStyle)) {
            file.setDowncounts(file.getDowncounts() + 1);
            fileMapper.updata(file);
        }
        //根据文件信息中文件名字 和 文件存储路径获取文件输入流
        //文件路径
        java.io.File path = new java.io.File("E:/schoolcloud/");
        System.out.println("根目录:" + path);
        String realpath = path + "/static/" + file.getUrlName();
        System.out.println("++++++文件请求下载的地址："+realpath);
        //获取文件输入流
        FileInputStream is = new FileInputStream(new java.io.File(realpath));
        //附件下载
        response.setHeader("content-disposition", openStyle + ";fileName=" + URLEncoder.encode(file.getOldName(), "UTF-8"));
        //获取响应输出流
        ServletOutputStream os = response.getOutputStream();
        //文件拷贝
        IOUtils.copy(is, os);
        IOUtils.closeQuietly(is);
        IOUtils.closeQuietly(os);
    }
    
    //更新下载次数
    @RequestMapping("/updateNum")
    public String updateNum(HttpServletRequest request) {
    	String rid = request.getParameter("id");
    	System.out.println("166id:"+rid);
    	Integer id = Integer.valueOf(rid);
        //查询所有的文件长度根据roomName
        Integer findNum = fileMapper.findNum(id);
        System.out.println("findNum:"+findNum);
        Integer downcounts = findNum + 1;
        System.out.println("downcounts:"+downcounts);
        int updataNum = fileMapper.updataNum(downcounts,id);
        System.out.println("updataNum:"+updataNum);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("updataNum",updataNum);
        System.out.println(updataNum);
        return JSONObject.toJSON(map).toString();
    }

    //搜索文件
    @RequestMapping("/searchFile")
    public List<File> searchFile( Model model,String fileName) {
        System.out.println("搜索的关键字："+fileName);
        //查询所有的文件信息
        List<File> userFiles = fileMapper.searchFile(fileName);
        //赋值
        model.addAttribute("files", userFiles);
        System.out.println(userFiles);
        return userFiles;
    }
    
    //文件删除
    @RequestMapping("dele")
    public String dele(HttpServletRequest request) {
    	String id = request.getParameter("id");
    	System.out.println("id:"+id);
        int dele = fileMapper.dele(id);
        //赋值
        Map<String, Object> map = new HashMap<String, Object>();
        //把值添加到map中
        map.put("dele",dele);
        System.out.println(dele);
        return JSONObject.toJSON(map).toString();
    }

}
