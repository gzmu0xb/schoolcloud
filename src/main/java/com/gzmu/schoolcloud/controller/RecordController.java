package com.gzmu.schoolcloud.controller;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.gzmu.schoolcloud.mapper.RecordMapper;
import com.gzmu.schoolcloud.mapper.RoomStudentMapper;
import com.gzmu.schoolcloud.model.Record;
import com.gzmu.schoolcloud.model.RoomStudent;

@Controller
public class RecordController {

	@Autowired
	private RecordMapper recordMapper;
	@Autowired
	private RoomStudentMapper roomStudentMapper;

	@ResponseBody
	@RequestMapping(value ="addRecord" ,produces = "text/html;charset=UTF-8")
	public String addRecord(HttpServletRequest request,HttpServletResponse response) throws IOException{
		//通过request请求获取的输入框中的值
		String roomName = request.getParameter("roomName");
		String score = request.getParameter("score");
		String openid = request.getParameter("openid");
		System.out.println("此次答题的分数和roomName"+score+"，"+roomName);
		String timeStr = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(new Date());
		System.out.println("此次答题的分数，roomName，时间："+score+"，"+roomName+"，"+timeStr);
		//返回值给微信小程序
		Writer out= new BufferedWriter(new OutputStreamWriter(response.getOutputStream()));
		try {
			out.write("Hello");
			out.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("55openid："+openid);
		//添加积分：分数（60-70],(70-80],(80-90],(90-100]各加10，20，30，40
		Integer num = Integer.valueOf(score);
		if(num != null) {
			//设置积分值
			Integer grade = 0;
			if(num >60 && num<=70) {
				grade = 10;
			}else if(num >70 && num<=80) {
				grade = 20;
			}else if(num >80 && num<=90) {
				grade = 30;
			}else if(num >90 && num<=100) {
				grade = 40;
			}else {
				grade = 0;
			}
			RoomStudent student = new RoomStudent();
			student.setRmName(roomName);
			student.setOpenid(openid);
			student.setGrade(grade);
			//查询当前工作室			
			int updateGrade = roomStudentMapper.updateGrade(student);
			System.out.println("77updateGrade:"+updateGrade);
		}
		
		//创建一个新的对象
		Record addRecord = new Record();
		addRecord.setTime(timeStr);
		addRecord.setRoomName(roomName);
		addRecord.setScore(score);
		addRecord.setOpenid(openid);
		//创建一个放数据的map
		Map<String, Object> map = new HashMap<String, Object>();
		int re = recordMapper.addRecord(addRecord);
		if (re==1){
			System.out.println("add Record success");
			map.put("result",true);
		}else{
			System.out.println("add Record fail");
			map.put("result",false);
		}
		return JSONObject.toJSON(map).toString();
	}

	/**
	 * 查询所有的记录信息（分页显示）
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "selectAllRecord", produces="text/html;charset=UTF-8")
	public String selectAllBack(HttpServletRequest request){
		String openid = request.getParameter("openid");
		String nu = request.getParameter("num");
		Integer num = Integer.valueOf(nu);
		Map<String, Object> map = new HashMap<String, Object>();
		List<Record> list = recordMapper.selectAllRecord(openid,num);
		System.out.println(list);
		map.put("list",list);
		return JSONObject.toJSON(map).toString();
	}
	
	/**
	 * 查询所有的记录信息长度
	 * @return
	 */
	@ResponseBody
	@RequestMapping(value = "selectAllRecordLength", produces="text/html;charset=UTF-8")
	public String selectAllRecordLength(HttpServletRequest request){
		String openid = request.getParameter("openid");
		Map<String, Object> map = new HashMap<String, Object>();
		int recordLength = recordMapper.RecordLength(openid);
		System.out.println(recordLength);
		map.put("recordLength",recordLength);
		return JSONObject.toJSON(map).toString();
	}
}
